import javafx.animation.*;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;

import javafx.scene.layout.HBox;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import java.lang.Thread;

public class GameVisual extends Application {

  private int playerX;
  private int playerY;

  private int dealerX;
  private int dealerY;

  private boolean gameEnded;

  private Deck deck;
  private BlackjackHand player;
  private BlackjackHand dealer;

  private Text message = new Text();
  private Text dealerM = new Text();
  private Text playerM = new Text();


  Button hitButton = new Button("HIT");
  Button stayButton = new Button("STAY");

  private Pane root = new Pane();

  class hitHandler implements EventHandler<ActionEvent> {
    public void handle(ActionEvent event) {


      if(!gameEnded){
        addPlayerCard(deck.deal());

        if(player.isBust()){
          gameOver();
          message.setText("   *****  You Lost  *****");
        }
      }
    }
  }

  class stayHandler implements EventHandler<ActionEvent> {
    public void handle(ActionEvent event) {

      try{
        dealer();
      }
      catch (InterruptedException e){
        System.err.println(e);
      }
    }
  }



  @Override
  public void start(Stage primaryStage) throws InterruptedException{

    // creating a clean game pane
    createGamePane();



    Scene scene = new Scene(root);
    primaryStage.setResizable(false);
    primaryStage.setTitle("BlackJack");
    primaryStage.setScene(scene);
    primaryStage.show();
  }




  private void createGamePane(){

    gameEnded = false;
    deck = new Deck();
    deck.shuffle();

    player = new BlackjackHand();
    dealer = new BlackjackHand();

    playerX = 300;
    playerY = 290;

    dealerX = 300;
    dealerY = 25;

    Region background = new Region();
    background.setPrefSize(640, 400);
    background.setStyle("-fx-background-color: rgba(41, 116, 50, 1)");
    root.getChildren().add(background);


    //main text field
    message.setText(" ");
    message.setLayoutX(20);
    message.setLayoutY(180);
    message.setStyle("-fx-font: 24 arial;");
    root.getChildren().add(message);

    //dealer text field
    dealerM.setText(" ");
    dealerM.setLayoutY(70);
    dealerM.setLayoutX(520);
    dealerM.setStyle("-fx-font: 42 arial;");
    root.getChildren().add(dealerM);

    //player text field
    playerM.setText(" ");
    playerM.setLayoutX(520);
    playerM.setLayoutY(330);
    playerM.setStyle("-fx-font: 42 arial;");
    root.getChildren().add(playerM);

    //buttons
    Button restartButton = new Button("RESTART");
    restartButton.setLayoutX(120);
    restartButton.setLayoutY(230);
    restartButton.setOnAction(event -> {
      root.getChildren().clear();
      createGamePane();
    });
    root.getChildren().add(restartButton);

    hitButton.setDisable(false);
    hitButton.setLayoutX(370);
    hitButton.setLayoutY(190);
    hitButton.setOnAction(new hitHandler());
    root.getChildren().add(hitButton);

    stayButton.setDisable(false);
    stayButton.setLayoutX(420);
    stayButton.setLayoutY(190);
    stayButton.setOnAction(new stayHandler());
    root.getChildren().add(stayButton);

    addPlayerCard(deck.deal());
    addPlayerCard(deck.deal());

  }





  private void addPlayerCard(Card card){
    player.add(card);

    //If player has more than 4 cards
    //Move his score above cards,
    //So cards does not cover the text
    if(player.size() > 4){
      playerM.setLayoutX(360);
      playerM.setLayoutY(270);
    }

    ImageView current = new ImageView(new FancyCard(card.toString()).getImage());
    playerX += 25;

    root.getChildren().add(current);

      //Animation
      TranslateTransition translate = new TranslateTransition(Duration.millis(500));
      translate.setToX(playerX);
      translate.setToY(playerY);
      translate.play();

      RotateTransition rotate = new RotateTransition(Duration.millis(500));
      rotate.setToAngle(180);

      ParallelTransition transition = new ParallelTransition(current, translate, rotate);
      transition.play();

    if(player.value() > 21)
      playerM.setText("BUST");
    else
      playerM.setText(Integer.toString(player.value()));
  }




  private void addDealerCard(Card card) throws InterruptedException{
    dealer.add(card);
    dealerM.setText(Integer.toString(dealer.value()));

    //If dealer has more than 4 cards
    //Move his score above cards,
    //So cards does not cover the text
    if(dealer.size() > 4){
      dealerM.setLayoutX(360);
      dealerM.setLayoutY(165);
    }

    ImageView current = new ImageView(new FancyCard(card.toString()).getImage());
    dealerX += 25;

    root.getChildren().add(current);



    //Animation
    TranslateTransition translate = new TranslateTransition(Duration.millis(500));
    translate.setToX(dealerX);
    translate.setToY(dealerY);

    RotateTransition rotate = new RotateTransition(Duration.millis(500));
    rotate.setToAngle(180);

    ParallelTransition transition = new ParallelTransition(current, translate, rotate);
    transition.play();

  }




  private void dealer() throws InterruptedException {

    while(!player.isBust()){

      if(deck.size() < 1)
        break;
      addDealerCard(deck.deal());

      if(dealer.isBust()){
        dealerM.setText("BUST");
        message.setText("!!!!!     You Win    !!!!!");
        gameOver();
        break;
      }

      if(dealer.value() >= 17){
        gameOver();
        if(dealer.value() == player.value()){
          message.setText("!!!!!           Tie          !!!!!");
        }
        else if(player.value() > dealer.value()){
          message.setText("!!!!!         You Win        !!!!!");
        }
        else{
          message.setText("   *****  You Lost  *****");
        }
        break;
      }
    }

  }





  private void gameOver(){
    hitButton.setDisable(true);
    stayButton.setDisable(true);
    gameEnded = true;
  }





  public static void main(String[] args) {
    launch(args);
  }

}
