import java.util.Scanner;
import java.lang.Thread;


class Game{
  public static void main(String[] args) throws InterruptedException {

    int total = 0;
    int playerWins = 0;
    int dealerWins = 0;
    int tied = 0;


    //game cycle
    char choice = ' ';
    while(choice != 'e'){
      ++total;

      BlackjackHand player = new BlackjackHand();
      BlackjackHand dealer = new BlackjackHand();

      Deck deck = new Deck();
      deck.shuffle();

      Scanner input = new Scanner(System.in);


      //dealing the first card to player
      player.add(deck.deal());
      System.out.println(player.toString());
      System.out.printf("[H]it or [S]tand? ");
      choice = input.next().charAt(0);

      //cycle for player to hit or stand
      while(choice != 's'){

        //dealing one more card
        player.add(deck.deal());
        System.out.println(player.toString());

        if(player.isBust()){
          System.out.println("!!!!!     BUST     !!!!!");
          System.out.println("***** Dealer  Wins *****");
          ++dealerWins;
          break;
        }

        System.out.printf("[H]it or [S]tand? ");
        choice = input.next().charAt(0);
      }

      //dealing for the Dealer

      while(!player.isBust()){

        dealer.add(deck.deal());
        System.out.println("**********************************");
        System.out.printf("       Your cards:     ");
        System.out.println(player.toString());
        System.out.println("**********************************");
        System.out.printf("     Dealer's cards:   ");
        System.out.println(dealer.toString());
        System.out.println("**********************************");
        System.out.printf("\n\n\n");

        if(dealer.isBust()){
          System.out.println("*****  Dealer BUSTS  *****");
          System.out.println("!!!!!     You Win    !!!!!");
          ++playerWins;
          break;
        }

        if(dealer.value() >= 17){
          System.out.println("*****  Dealer stops dealing  *****");
          if(dealer.value() == player.value()){
            System.out.println("!!!!!           Tie          !!!!!");
            ++tied;
          }
          else if(player.value() > dealer.value()){
            System.out.println("!!!!!         You Win        !!!!!");
            ++playerWins;
          }
          else{
            System.out.println(":(((   Unfortunitely you lost  ))):");
            System.out.println("       Better Luck next time!      ");
            ++dealerWins;
          }
          break;
        }
        Thread.sleep(500);
      }

      //choice to exit, cleaning hand, refreshing deck;
      System.out.println("To continue type in any simbol");
      System.out.println("       To exit type 'e'       ");
      choice = input.next().charAt(0);

    }

    System.out.println("Statistics:\n");
    System.out.println("Total played: " + total);
    System.out.println("You won:      " + playerWins);
    System.out.println("Dealer won:   " + dealerWins);
    System.out.println("Tied games:   " + tied);
  }
}
