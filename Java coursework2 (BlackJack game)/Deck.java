import java.util.Collections;
import java.util.LinkedList;

class Deck extends CardCollection{

  public Deck(){
    super();
    for(int i=1; i<=13; i++){
      String rank = "";

      if(i == 1){
        rank = "A";
      }
      else if(i == 10){
        rank = "T";
      }

      else if(i == 11){
        rank = "J";
      }

      else if(i == 12){
        rank = "Q";
      }

      else if(i == 13){
        rank = "K";
      }

      else{
        rank = Integer.toString(i);
      }
      add(new Card(rank+"C"));
      add(new Card(rank+"D"));
      add(new Card(rank+"H"));
      add(new Card(rank+"S"));
    }
    sort();
  }

  public void shuffle(){
    Collections.shuffle(cards);
  }

  public Card deal(){
    if(isEmpty()){
      throw new CardException("Cannot deal because the deck is empty");
    }
    Card temp = cards.get(0);
    cards.remove(0);
    return temp;
  }

}
