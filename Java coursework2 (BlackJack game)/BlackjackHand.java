class BlackjackHand extends CardCollection{

  public BlackjackHand(){
    super();
  }

  public BlackjackHand(String hand){
    String[] handCards = hand.split(" ");
    for (String x : handCards){
      add(new Card(x));
    }
  }

  public void discard(Deck deck){
    if(isEmpty())
      throw new CardException("Discard is called on empty hand");

    for(Card x : cards){
      deck.add(x);
    }
    discard();
  }

  public String toString(){
    String name = "";
    for(Card x : cards){
      name += x.toString() + " ";
    }
    return name.trim();
  }

  public boolean isNatural(){
    return (value() == 21 && size() == 2);
  }

  public boolean isBust(){
    return value() > 21;
  }

  @Override
  public void add(Card card){
    if(isBust())
      throw new CardException("Cannot add card to already bust hand");
    else
      cards.add(card);
  }

  @Override
  public int value() {
    int sum = 0;
    boolean ace = false;
    for (Card card: cards) {
      if(card.value() == 1){
        ace = true;
      }
      sum += card.value();
    }

    if(ace && (sum+10 <= 21)){
      sum += 10;
    }
    return sum;
  }
}
