#ifndef _growingHeader
#define _growingHeader

#include "struct.h"

// split a leaf nodes into 4 children
void makeChildren( Node *parent );

//grows tree by one level
void growTree(Node *parent);

// make a node at given location (x,y) and level
Node *makeNode( double x, double y, int level );

//changing MAXIMUM_LEVEL (level 10 is default)
void setMaxLevel(int x);

#endif
