//Header for defining the structure.

#ifndef _structHeader
#define _structHeader

struct qnode {
  int level;
  double xy[2];
  struct qnode *child[4];
};
typedef struct qnode Node;

#endif
