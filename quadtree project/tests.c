#include "stdio.h"

#include "quadTreeGenerator.h"
#include "destroyTree.h"
#include "struct.h"
#include "writing.h"
#include "growing.h"
#include "struct.h"

void nonUniformLevel3(Node *head){

  makeChildren(head);

  for(int i=0;i<4;i++){
    makeChildren(head->child[i]);
    }
  makeChildren(head->child[3]->child[0]);
}

void task1(){
  // make the head node
  Node *head;
  head = makeNode( 0.0,0.0, 0 );

  nonUniformLevel3(head);

  //write tree to show that tree is of level 3
  writeTree(head);

  //destroying the trees
  destroyTree(head);
}

void task2(){

  Node *head;
  head = makeNode( 0.0,0.0, 0 );

  nonUniformLevel3(head);

  growTree(head);

  writeTree(head);
  destroyTree(head);
}

void task3(int max){

  Node *head;
  head = makeNode( 0.0,0.0, 0 );


  nonUniformLevel3(head);
  setMaxLevel(max);

  growTree(head);
  writeTree(head);
  destroyTree(head);
}

void task4(int choi, double tol){

  Node *head;
  head = makeNode( 0.0,0.0, 0 );

  setMaxLevel(10);

  //seting quadtree to full level 2
  for(int i=0;i<2;i++){
    growTree(head);
  }

  addingChildrenIfNeeded(head,tol,choi);

  writeTree(head);
  destroyTree(head);
}
