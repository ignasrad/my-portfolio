#include "stdio.h"
#include "stdlib.h"
#include "math.h"

#include "tests.h"



int main( int argc, char **argv ) {
  task1();
  task2();
  task3(4); // specifying max level 4.
  task4(1,0.2); // specifying choice 1 and tolerance 0.2
}
