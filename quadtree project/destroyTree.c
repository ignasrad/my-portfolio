#include "stdlib.h"
#include "struct.h"
#include "stdio.h"


#include "destroyTree.h"


void destroyTree(Node *top){

    if(top->child[0] == NULL){
      free(top);
      return;
    }

    int i;
    for(i=0;i<4;i++){
      destroyTree(top->child[i]);
    }

    free(top);
    return;
}
