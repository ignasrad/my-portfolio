#include "stdio.h"
#include "stdlib.h"
#include "math.h"


#include "struct.h"
#include "growing.h"

//maximum level parameter
int MAXIMUM_LEVEL = 10;

void growTree(Node *parent){

  //max level checker
  if(parent->level == MAXIMUM_LEVEL){
    return;
  }

  if(parent->child[0] == NULL){
    makeChildren(parent);
    return;
  }

  int i;
  for(i=0;i<4;i++){
    growTree(parent->child[i]);
  }
  return;
}

void makeChildren( Node *parent ) {



  double x = parent->xy[0];
  double y = parent->xy[1];

  int level = parent->level;

  double hChild = pow(2.0,-(level+1));

  parent->child[0] = makeNode( x,y, level+1 );
  parent->child[1] = makeNode( x+hChild,y, level+1 );
  parent->child[2] = makeNode( x+hChild,y+hChild, level+1 );
  parent->child[3] = makeNode( x,y+hChild, level+1 );

  return;
}

Node *makeNode( double x, double y, int level ) {

  int i;

  Node *node = (Node *)malloc(sizeof(Node));

  node->level = level;

  node->xy[0] = x;
  node->xy[1] = y;

  for( i=0;i<4;++i )
    node->child[i] = NULL;

  return node;
}

void setMaxLevel (int x){
  MAXIMUM_LEVEL = x;
}
