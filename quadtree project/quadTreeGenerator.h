#ifndef _quadTreeGenerator
#define _quadTreeGenerator


#include "struct.h"
#include "stdbool.h"

double dataFunction( double x, double y, int choice );

bool indicator( Node *node, double tolerance, int choice );

void addingChildrenIfNeeded( Node *node, double tolerance, int choice );

void countingFalseResults();
int getFalseResults();

#endif
