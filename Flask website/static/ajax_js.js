$(document).ready(function() {

    $("a.save").on("click", function() {

      var id = $(this).attr('id')
      var clicked = $(this);

        $.ajax({
            // Specify the endpoint URL the request should be sent to.
            url: '/save',
            // The request type.
            type: 'POST',
            // The data, which is now most commonly formatted using JSON because of its
            // simplicity and is native to JavaScript.
            data: JSON.stringify({ id: id}),
            // Specify the format of the data which will be sent.
            contentType: "application/json; charset=utf-8",
            // The data type itself.
            dataType: "json",
            // Define the function which will be triggered if the request is received and
            // a response successfully returned.
            success: function(response){
                if(clicked.children()[0].innerHTML.trim() == 'Follow'){
                  clicked.children()[0].innerHTML = 'Unfollow'
                }
                else{
                  clicked.children()[0].innerHTML = 'Follow'
                }

                console.log(clicked.children()[0].innerHTML.trim());
            },
            // The function which will be triggered if any error occurs.
            error: function(error){
                console.log(error);
            }
        });
    });

    $("a.vote").on("click", function() {

        var id = $(this).attr('id')
        var clicked = $(this);

        $.ajax({
            // Specify the endpoint URL the request should be sent to.
            url: '/like',
            // The request type.
            type: 'POST',
            // The data, which is now most commonly formatted using JSON because of its
            // simplicity and is native to JavaScript.
            data: JSON.stringify({ id: id}),
            // Specify the format of the data which will be sent.
            contentType: "application/json; charset=utf-8",
            // The data type itself.
            dataType: "json",
            // Define the function which will be triggered if the request is received and
            // a response successfully returned.
            success: function(response){
                console.log("Endorsed id: " + response.id);
                clicked.children()[1].innerHTML = response.votes;

            },
            // The function which will be triggered if any error occurs.
            error: function(error){
                console.log(error);
            }
        });
    });

    $(".modal-footer .btn-primary").on("click", function() {
      var id = $(this).attr('id')
      var clicked = $(this);
      var field = $('.input-group').find('#'+id);
      var text = field.val();

        $.ajax({
            // Specify the endpoint URL the request should be sent to.
            url: '/comment',
            // The request type.
            type: 'POST',
            // The data, which is now most commonly formatted using JSON because of its
            // simplicity and is native to JavaScript.
            data: JSON.stringify({ id: id, text: text}),
            // Specify the format of the data which will be sent.
            contentType: "application/json; charset=utf-8",
            // The data type itself.
            dataType: "json",
            // Define the function which will be triggered if the request is received and
            // a response successfully returned.
            success: function(response){
                var newCom = "<p><b>"+ response.name +"</b></p><p>"+text+"</p><hr>";
                $(".modal-body form").before(newCom);
                console.log("success");
                field.val("");
            },
            // The function which will be triggered if any error occurs.
            error: function(error){
                console.log(error);
            }
        });
    });
});
