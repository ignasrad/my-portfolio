import java.util.*;
import java.io.*;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

class Track{

  private ArrayList<Point> sequence;

  public Track(){
    sequence = new ArrayList<>();
  }

  public void readFile(String filename) throws FileNotFoundException {
    String line = "";
    File file = null;
    String path = "../data/" + filename;
    sequence.clear();

    file = new File(path);

    Scanner input = new Scanner(file);


    // TODO what if csv file is empty ??? !!!


    line = input.nextLine();

    String[] headerLine = line.split(",");

    while(input.hasNextLine()){
      line = input.nextLine();

      String[] seperated = line.split(",");

      //checking if array is full
      if (seperated.length != 4){
        throw new GPSException("Some record does not contain enough values");
      }
      Point temp = new Point(ZonedDateTime.parse(seperated[0]),
                              Double.parseDouble(seperated[1]),
                              Double.parseDouble(seperated[2]),
                              Double.parseDouble(seperated[3]));
      add(temp);
    }
  }

  public void add(Point temp){
    sequence.add(temp);
  }

  public int size(){
    return sequence.size();
  }

  public Point get(int pos){

    if(pos >= size() || pos < 0){
      throw new GPSException("Requested point is not within the allowed range");
    }
    else
      return sequence.get(pos);
  }


  public Point lowestPoint(){

    if(sequence.size() < 1)
      throw new GPSException("There arent enough points to find lowest");


    Point min = sequence.get(0);

    for(Point temp : sequence){
      if( temp.getElevation() < min.getElevation())
        min = temp;
    }
    return min;
  }

  public Point highestPoint(){

    if(sequence.size() < 1)
      throw new GPSException("There arent enough points to find highest");


    Point max = sequence.get(0);

    for(Point temp : sequence){
      if( temp.getElevation() > max.getElevation())
        max = temp;
    }
    return max;
  }

  public double totalDistance(){

    if(sequence.size() < 2)
      throw new GPSException("There arent enough points to count totalDistance");

    double distance=0;

    for(int i=0; i+1 < sequence.size(); i++){
      distance += Point.greatCircleDistance(sequence.get(i),sequence.get(i+1));
    }
    return distance;
  }

  public double averageSpeed(){

    if(sequence.size() < 2)
      throw new GPSException("There arent enough points to count averageSpeed");


    long seconds = ChronoUnit.SECONDS.between(sequence.get(0).getTime(),
                                              sequence.get(size()-1).getTime());
    return totalDistance()/seconds;
  }

  public double traveledDistance(int pos){
    if(sequence.size() < 2)
      throw new GPSException("There arent enough points to count totalDistance");

    double distance=0;

    for(int i=0; i+1 <= pos; i++){
      distance += Point.greatCircleDistance(sequence.get(i),sequence.get(i+1));
    }
    return distance;
  }
}
