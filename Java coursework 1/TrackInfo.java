import java.time.*;
import java.io.FileNotFoundException;

class TrackInfo{
  public static void main(String[] args) throws FileNotFoundException{

    Track trackObj = new Track();
    //reading file
    try{
      trackObj.readFile(args[0]);
    }
    catch (ArrayIndexOutOfBoundsException e){
      System.err.println("CSV filename should be suplied from the command line");
      System.exit(1);
    }
    catch (FileNotFoundException e) {
      System.err.println("File is not found");
      System.exit(1);
    }
    catch (GPSException e){
      System.err.println(e);
      System.exit(1);
    }

    System.out.printf("%d points in track\n", trackObj.size());

    //getting lowestPoint
    try{
      System.out.printf("Lowest point is %s\n", trackObj.lowestPoint().toString());
    }
    catch (GPSException e) {
      System.err.println(e);
      System.exit(2);
    }

    //getting highestPoint
    try{
      System.out.printf("Highest point is %s\n", trackObj.highestPoint().toString());
    }
    catch (GPSException e) {
      System.err.println(e);
      System.exit(2);
    }

    //getting totalDistance
    try{
      System.out.printf("Total distance = %.3f km\n", trackObj.totalDistance()/1000);
    }
    catch (GPSException e) {
      System.err.println(e);
      System.exit(2);
    }

    //getting averageSpeed
    try{
      System.out.printf("Average speed = %.3f m/s\n", trackObj.averageSpeed());
    }
    catch (GPSException e) {
      System.err.println(e);
      System.exit(2);
    }
  }
}
