import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
import java.io.FileNotFoundException;



import java.util.*;

public class ElevationPlot extends Application {


    @Override
    public void start(Stage primary) throws FileNotFoundException{
      Parameters params = getParameters();
      List<String> list = params.getRaw();
      String filename;

      filename = list.get(0);

      Track trackObj = new Track();

      trackObj.readFile(filename);

      primary.setTitle("Elevation chart");

      // creating axis

      final NumberAxis x = new NumberAxis();
      final NumberAxis y = new NumberAxis();

      x.setLabel("Distance (m)");
      y.setLabel("Elevation (m)");

      //creating line chart

      final LineChart<Number,Number> chart =
              new LineChart<Number,Number>(x,y);
      chart.setTitle("Elevation Plot");

      //creating a sequence of points
      XYChart.Series<Number, Number>  seq = new XYChart.Series<>();
      seq.setName(filename);

      // adding Data
      for(int i=0; i<trackObj.size(); i++){
        seq.getData().add(new XYChart.Data<Number,Number>(trackObj.traveledDistance(i),
                                                          trackObj.get(i).getElevation()) );
      }


      chart.setCreateSymbols(false);
      chart.getData().add(seq);

      Scene scene = new Scene(chart,800, 600);
      primary.setScene(scene);
      primary.show();
    }

    public static void main(String[] args) throws FileNotFoundException{
      if(args.length < 1){
        throw new FileNotFoundException("Pass the filename from console");
      }
      launch(args);
    }
}
