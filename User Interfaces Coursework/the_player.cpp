//
// Created by twak on 11/11/2019.
//

#include "the_player.h"
#include <QtWidgets>
#include <iostream>
#include <vector>

using namespace std;

//ThePlayer::ThePlayer()
//{
//    createWidgets();
//    nextPage();
//    makeConnections();
//}
//void ThePlayer::createWidgets()
//{
//    nextButton = new QPushButton("Next");
//}





void ThePlayer::nextPage() {

    int temp = A[infos->size()-1], i;
    for (i = infos->size()-1; i > 0; i--)
        A[i] = A[i - 1];

    A[0] = temp;

    buttons -> at(0) -> init( & infos -> at (A.at(0)) );
    buttons -> at(1) -> init( & infos -> at (A.at(1) ) );
    buttons -> at(2) -> init( & infos -> at (A.at(2)) );
    buttons -> at(3) -> init( & infos -> at (A.at(3)) );
    /*if(n_count+1 == infos->size()){
        buttons -> at(0) -> init( & infos -> at (n_count ) );
        buttons -> at(1) -> init( & infos -> at (0 ) );
        buttons -> at(2) -> init( & infos -> at (1 ) );
        buttons -> at(3) -> init( & infos -> at (2 ) );
    }

    else if(n_count+2 == infos->size()){
        buttons -> at(0) -> init( & infos -> at (n_count ) );
        buttons -> at(1) -> init( & infos -> at (n_count + 1 ) );
        buttons -> at(2) -> init( & infos -> at (0 ) );
        buttons -> at(3) -> init( & infos -> at (1 ) );
    }

    else if(n_count+3 == infos->size()){
        buttons -> at(0) -> init( & infos -> at (n_count ) );
        buttons -> at(1) -> init( & infos -> at (n_count + 1 ) );
        buttons -> at(2) -> init( & infos -> at (n_count + 2 ) );
        buttons -> at(3) -> init( & infos -> at (0 ) );
    }
    else if(n_count == infos->size()){
        n_count = 0;
        buttons -> at(0) -> init( & infos -> at (n_count ) );
        buttons -> at(1) -> init( & infos -> at (n_count + 1  ) );
        buttons -> at(2) -> init( & infos -> at (n_count + 2) );
        buttons -> at(3) -> init( & infos -> at (n_count + 3) );
    }

    else{

    }*/
   // jumpTo(buttons -> at(0) -> info);
    //n_count++;
}


void ThePlayer::prevPage() {
    int temp = A[0], i;
    for (i = 0; i < infos->size()-1; i++)
        A[i] = A[i + 1];

    A[i] = temp;

    buttons -> at(0) -> init( & infos -> at (A.at(0)) );
    buttons -> at(1) -> init( & infos -> at (A.at(1) ) );
    buttons -> at(2) -> init( & infos -> at (A.at(2)) );
    buttons -> at(3) -> init( & infos -> at (A.at(3)) );
    /*if(p_count+4 == infos->size()){
        buttons -> at(0) -> init( & infos -> at (p_count + 3 ) );
        buttons -> at(1) -> init( & infos -> at (0 ) );
        buttons -> at(2) -> init( & infos -> at (1 ) );
        buttons -> at(3) -> init( & infos -> at (2 ) );
    }

    else if(p_count+2 == infos->size()){
        buttons -> at(0) -> init( & infos -> at (p_count - 1 ) );
        buttons -> at(1) -> init( & infos -> at (p_count ) );
        buttons -> at(2) -> init( & infos -> at (p_count + 1 ) );
        buttons -> at(3) -> init( & infos -> at (0 ) );
    }

    else if(p_count+3 == infos->size()){
        buttons -> at(0) -> init( & infos -> at (p_count + 1) );
        buttons -> at(1) -> init( & infos -> at (p_count + 2 ) );
        buttons -> at(2) -> init( & infos -> at (0 ) );
        buttons -> at(3) -> init( & infos -> at (1 ) );
    }
    else if(p_count == infos->size()){
        p_count = 0;
        buttons -> at(0) -> init( & infos -> at (p_count + 2  ) );
        buttons -> at(1) -> init( & infos -> at (p_count + 3  ) );
        buttons -> at(2) -> init( & infos -> at (p_count + 4) );
        buttons -> at(3) -> init( & infos -> at (p_count + 5) );
    }

    else{
        buttons -> at(0) -> init( & infos -> at (p_count + 2 ) );
        buttons -> at(1) -> init( & infos -> at (p_count + 3 ) );
        buttons -> at(2) -> init( & infos -> at (p_count + 4 ) );
        buttons -> at(3) -> init( & infos -> at (p_count + 5 ) );
    }*/
    // jumpTo(buttons -> at(0) -> info);
    //p_count++;
}


void ThePlayer::InitialiseVector(vector<int>B)
{
        int i;
        for(i = 0; i < infos->size(); i++)
        {
            B.push_back(i);
        }
        A = B;
}


// all buttons have been setup, store pointers here
void ThePlayer::setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i){
    buttons = b;
    infos = i;
    jumpTo(buttons -> at(0) -> info);
}



void ThePlayer::playStateChanged (QMediaPlayer::State ms) {
    switch (ms) {
        case QMediaPlayer::State::StoppedState:
            play(); // starting playing again...
    }
}




void ThePlayer::changeState() {
    if(this->state() == QMediaPlayer::State::PlayingState)
        this->pause();
    else if(this->state() == QMediaPlayer::State::PausedState)
        this->play();
}

void ThePlayer::jumpTo (TheButtonInfo* button) {
    setMedia( * button -> url);
    play();
}

//ieskoti
void ThePlayer::getSearchText(){
    vector<TheButtonInfo*> result = vector<TheButtonInfo*>();
    for(int i = 0; i < infos->size(); i++){
        TheButtonInfo* temp = &infos -> at(i);

        for(int j = 0; j < temp -> tags.size(); j++){
            if( temp->tags.at(j)->compare(search) == 0 ) {
                result.push_back(temp);
            }
        }
    }
    if(result.size() == 0){
        nextPage();
    }

//    buttons -> clear();

    for(int i = 0; i < result.size(); i++){
        cout<<i<<endl;
        buttons -> at(i) -> init( result.at (i ) );
        buttons -> at(i)->setVisible(true);
        //player->setContent(buttons, & result);
    }

    for(int j = result.size(); j < 4; j++){
        buttons -> at(j)->setVisible(false);
    }
}

void ThePlayer::setSearchText(const QString& s){
    search = s.toStdString();
}
//ends

void ThePlayer::getGenre1(){
    vector<TheButtonInfo*> result = vector<TheButtonInfo*>();
    for(int i = 0; i < infos->size(); i++){
        TheButtonInfo* temp = &infos -> at(i);

        for(int j = 0; j < temp -> tags.size(); j++){
            if( temp->tags.at(j)->compare("nature") == 0 ) {
                result.push_back(temp);
            }
        }
    }
    if(result.size() == 0){
        nextPage();
    }

//    buttons -> clear();

    for(int i = 0; i < result.size(); i++){
        cout<<i<<endl;
        buttons -> at(i) -> init( result.at (i ) );
        buttons -> at(i)->setVisible(true);
        //player->setContent(buttons, & result);
    }

    for(int j = result.size(); j < 4; j++){
        buttons -> at(j)->setVisible(false);
    }
}

void ThePlayer::getGenre2(){
    vector<TheButtonInfo*> result = vector<TheButtonInfo*>();
    for(int i = 0; i < infos->size(); i++){
        TheButtonInfo* temp = &infos -> at(i);

        for(int j = 0; j < temp -> tags.size(); j++){
            if( temp->tags.at(j)->compare("gopro") == 0 ) {
                result.push_back(temp);
            }
        }
    }
    if(result.size() == 0){
        nextPage();
    }

//    buttons -> clear();

    for(int i = 0; i < result.size(); i++){
        cout<<i<<endl;
        buttons -> at(i) -> init( result.at (i ) );
        buttons -> at(i)->setVisible(true);
        //player->setContent(buttons, & result);
    }

    for(int j = result.size(); j < 4; j++){
        buttons -> at(j)->setVisible(false);
    }
}

void ThePlayer::getGenre3(){
    vector<TheButtonInfo*> result = vector<TheButtonInfo*>();
    for(int i = 0; i < infos->size(); i++){
        TheButtonInfo* temp = &infos -> at(i);

        for(int j = 0; j < temp -> tags.size(); j++){
            if( temp->tags.at(j)->compare("china") == 0 ) {
                result.push_back(temp);
            }
        }
    }
    if(result.size() == 0){
        nextPage();
    }

//    buttons -> clear();

    for(int i = 0; i < result.size(); i++){
        cout<<i<<endl;
        buttons -> at(i) -> init( result.at (i ) );
        buttons -> at(i)->setVisible(true);
        //player->setContent(buttons, & result);
    }

    for(int j = result.size(); j < 4; j++){
        buttons -> at(j)->setVisible(false);
    }
}




void ThePlayer::makeConnections() {
//    connect(nextPage, SIGNAL(clicked()), this, SLOT(nextPage()));
}

