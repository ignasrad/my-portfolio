//
// Created by sc18ctdt on 12/12/19.
//

#ifndef CW2_THE_WINDOW_H
#define CW2_THE_WINDOW_H

#include <QtWidgets>
#include <vector>
#include <string>
#include "the_button.h"
#include "the_player.h"

class the_window : public QWidget {
Q_OBJECT


public:

    the_window(vector<TheButtonInfo> vids) : QWidget()
    {
        videos = vids;
        //createWidgets();
        createMenu();
        createWidgets();

    }
    //void createMenu();
    int check=0;
    QPushButton* pauseButton = new QPushButton();
private:

    void createMenu();
    void createWidgets();
    //design
    QString url = R"(/home/ignas/Desktop/test/ui-cw2/Image_file/searchButt.svg)";
    QString url3 =R"(/home/ignas/Desktop/test/ui-cw2/Image_file/play.svg)";
    QString url2 =R"(/home/ignas/Desktop/test/ui-cw2/Image_file/pause.svg)";
    QString url4 =R"(/home/ignas/Desktop/test/ui-cw2/Image_file/backButton.svg)";
    QString url5 =R"(/home/ignas/Desktop/test/ui-cw2/Image_file/nextButton.svg)";
    QIcon Icon;
    QIcon Icon2;
    QIcon Icon3;
    QIcon Icon4;
    QIcon Icon5;
    //menu
    QString url6 = R"(/home/ignas/Desktop/test/ui-cw2/Image_file/banner2.png)";
    QIcon Icon6;

    QVBoxLayout* menuLayout = new QVBoxLayout();
    QHBoxLayout* menu1 = new QHBoxLayout();
    QHBoxLayout* menu2 = new QHBoxLayout();
    QPushButton* menuLbl = new QPushButton();
    QPushButton* menuBtn1 = new QPushButton("Show All");
    QPushButton* menuBtn2 = new QPushButton("Nature");
    QPushButton* menuBtn3 = new QPushButton("GoPro");
    QPushButton* menuBtn4 = new QPushButton("China");

    vector<TheButtonInfo> videos;
    //top level layout
    QVBoxLayout* top = new QVBoxLayout();
    QHBoxLayout *videoLayout = new QHBoxLayout();
    QHBoxLayout *layout = new QHBoxLayout();

    //buttons declaration
    vector<TheButton*> buttons;
    QPushButton* genre1 = new QPushButton("Nature");
    QPushButton* genre2 = new QPushButton("GoPro");
    QPushButton* genre3 = new QPushButton("China");
    QPushButton* genre4 = new QPushButton("Genre 2");
    QHBoxLayout * h_SearchBar = new QHBoxLayout();


    QPushButton* nextButton = new QPushButton();
    QPushButton* backButton = new QPushButton();
    QLineEdit* search = new QLineEdit();
    QPushButton* searchButton = new QPushButton();

    //player declaration
    ThePlayer *player = new ThePlayer;
    //volume slider declaration
    QSlider* slider = new QSlider(Qt::Horizontal);
    vector<int>B;
    //timer
    QTimer* mTimer = new QTimer();
    QSlider* bar = new QSlider(Qt::Horizontal);
    QLabel* time = new QLabel(QString());


public slots:
    //void createWidgets();
    void createWidgets2();
    void setTime();
    void setBarValue();
    void setLayout1();
    void setLayout2();
    void setLayout3();
    void setLayout4();
    void isChecked();


};


#endif //CW2_THE_WINDOW_H
