/*
 *
 *    ______
 *   /_  __/___  ____ ___  ___  ____
 *    / / / __ \/ __ `__ \/ _ \/ __ \
 *   / / / /_/ / / / / / /  __/ /_/ /
 *  /_/  \____/_/ /_/ /_/\___/\____/
 *              video for no reason
 *
 * 2811 cw2 November 2019 by twak
 */

#include <iostream>
#include <QApplication>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QMediaPlaylist>
#include <string>
#include <vector>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>
#include <QtCore/QFileInfo>
#include <QtWidgets/QFileIconProvider>
#include <QImageReader>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include "the_player.h"
#include "the_button.h"
#include "the_window.h"
#include <QtWidgets>
#include <QtGlobal>
#include <fstream>
using namespace std;

//ieskoti
vector<string*> readFile(string strFile) //Read the file into the vector function definition
{
    ifstream iFile;

    vector<string*> tags = vector<string*>();
    iFile.open(strFile); //Opens file

    string* tag = new string(); //tag

    while (iFile >> *tag) //While the file is copying tags
    {
        string* tag2 = new string(*tag); //tag
        tags.push_back(tag2); //Push tags
    }
    iFile.close(); //Close the input file
    return tags;
}
//end

// read in videos and thumbnails to this directory
vector<TheButtonInfo> getInfoIn (string loc) {

    vector<TheButtonInfo> out =  vector<TheButtonInfo>();
    QDir dir(QString::fromStdString(loc) );
    QDirIterator it(dir);

    while (it.hasNext()) { // for all files

        QString f = it.next();

        if (!f.contains(".png") && !f.contains(".txt")) { // if it isn't an image
            QString thumb = f.left( f .length() - 4) +".png";
            QString txt = f.left( f .length() - 4) +".txt";
            vector<string*> tags;
            //ieskoti
            if (QFile(txt).exists()) { // txt exists
                tags = readFile(txt.toStdString());
            }
            else{
                tags = vector<string*>();
            }
            //end

            if (QFile(thumb).exists()) { // but a png thumbnail exists
                QImageReader *imageReader = new QImageReader(thumb);
                QImage sprite = imageReader->read(); // read the thumbnail
                if (!sprite.isNull()) {
                    QIcon* ico = new QIcon(QPixmap::fromImage(sprite)); // voodoo to create an icon for the button
                    QUrl* url = new QUrl(QUrl::fromLocalFile( f )); // convert the file location to a generic url
                    out . push_back(TheButtonInfo( url , ico, tags  ) ); // add to the output list
                }
                else
                    cerr << "warning: skipping video because I couldn't process thumbnail " << f.toStdString() << endl;
            }
            else if (f.contains(".MOV") and f.contains(".mp4")) {
                cerr << "warning: skipping video because I couldn't find thumbnail " << thumb.toStdString() << endl;
            }
        }
    }

    return out;
}


int main(int argc, char *argv[]) {

    // let's just check that Qt is operational first
    cout << "Qt version: " << QT_VERSION_STR << endl;

    // create the Qt Application
    QApplication app(argc, argv);

    // collect all the videos in the folder

    vector<TheButtonInfo> videos;

    if (argc == 1)
        videos = getInfoIn("/tmp/2811/2811_videos/");
    else
        videos = getInfoIn( string(argv[1]) );

    if (videos.size() == 0) {
        cerr << "no videos found! download from https://vcg.leeds.ac.uk/wp-content/static/2811/the/videos.zip into /tmp/XXX, and update the code on line 77";
        exit(-1);
    }

//    //video and navigation layout
//    QHBoxLayout *videoLayout = new QHBoxLayout();
//    QVBoxLayout *navigationLayout = new QVBoxLayout();
//
//    // the widget that will show the video
//    QVideoWidget *videoWidget = new QVideoWidget;
//    videoLayout->addLayout(navigationLayout);
//    videoLayout->addWidget(videoWidget);
//
//    //Buttons for navigation
//    QLabel* navigationLabel = new QLabel("Navigation Tab");
//    QPushButton* genre1 = new QPushButton("Nature");
//    QPushButton* genre2 = new QPushButton("Genre 2");
//
//    navigationLayout->addWidget(navigationLabel);
//    navigationLayout->addWidget(genre1);
//    navigationLayout->addWidget(genre2);
//    navigationLayout->addStretch(1);
//
//
//  //      QLabel* durationLabel = new QLabel();
//    // the QMediaPlayer which controls the playback
///*
//    int h,m,s;
//    QLabel * timer = new QLabel();
//
//    player->getduration(playtime);
//
//    playtime /= 1000;
//    h = playtime/3600;
//    m = (playtime-h*3600)/60;
//    s = playtime-h*3600-m*60;
//
//    timer -> setText(QString("%1:%2:%3").arg(h).arg(m).arg(s));
//    timer -> setStyleSheet("color:black");
//    player->connect(player, SIGNAL(durationChanged(playtime)), player, SLOT(getduration(playtime)));
//
//    player->connect(player, SIGNAL(durationChanged(playtime)), timer, SLOT(getduration(timer)));
//*/
//
//
//    ThePlayer *player = new ThePlayer;
//    player->setVideoOutput(videoWidget);
//    // a row of buttons
//    QWidget *buttonWidget = new QWidget();
//    // a list of the buttons
//    vector<TheButton*> buttons;
//    // the buttons are arranged horizontally
//    QHBoxLayout *layout = new QHBoxLayout();
//
//    //back button
//    QPushButton* backButton = new QPushButton("Previous Page");
//    layout->addWidget(backButton);
//    backButton->setMinimumHeight(115);
//    backButton->setMaximumWidth(80);
//
//    // set a volume Slider
//    QSlider* slider = new QSlider(Qt::Horizontal);
//    slider->setRange(0,100);
//    slider->setValue(100);
//    slider->setTickPosition(QSlider::TicksBelow);
//    slider->setTickInterval(1);
//    player->setVolume(slider->value());
//    slider->connect(slider, SIGNAL(valueChanged(int)),player,SLOT(setVolume(int)));
//
//    buttonWidget->setLayout(layout);
//
//    //make a timer
////    QTimer * timer = new QTimer();
////    timer ->connect(timer, &QTimer::timeout, player, QOverload<>::of(&AnalogClock::update));
////    timer ->start(0);
//
//    // create the four buttons
//    for ( int i = 0; i < 4; i++ ) {
//        TheButton *button = new TheButton(buttonWidget);
//        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.
//        buttons.push_back(button);
//        layout->addWidget(button);
//        button->init(&videos.at(i));
//    }
//
//    //ieskoti
//    QLineEdit* search = new QLineEdit();
//    QPushButton* searchButton = new QPushButton("Search");
//    search->connect(search,SIGNAL(textChanged(const QString&)), player, SLOT(setSearchText(const QString&)));
//    searchButton->connect(searchButton, SIGNAL(clicked()),player,SLOT(getSearchText()));
//
//    //navigation
//    genre1->connect(genre1, SIGNAL(clicked()), player, SLOT(getGenre1()));
//
//    QPushButton* nextButton = new QPushButton("Next Page");
//    nextButton->connect(nextButton, SIGNAL(clicked()),player,SLOT(nextPage()));
//    backButton->connect(backButton, SIGNAL(clicked()),player,SLOT(prevPage()));
//    QPushButton* pauseButton = new QPushButton("Play/Pause");
//    pauseButton->connect(pauseButton, SIGNAL(clicked()),player,SLOT(changeState()));
//    nextButton->setMinimumHeight(115);
//    nextButton->setMaximumWidth(80);
//
//
//
//    // tell the player what buttons and videos are available
//    player->setContent(&buttons, & videos);
//    //durationLabel->setText(player->metaData(QMediaMetaData::Duration).toString());
///*
//    // create the main window and layout
//    QWidget window;*/
//    the_window window(videos);
//    QVBoxLayout *top = new QVBoxLayout();
//    window.setLayout(top);
//    window.setWindowTitle("tomeo");
//    window.setMinimumSize(800, 680);
//
//    //ieskoti
//    search -> setMaximumWidth(300);
//    searchButton -> setMaximumWidth(60);
//    top->addWidget(search);
//    top->addWidget(searchButton);
//
//    // add the video and the buttons to the top level widget
//    top->addLayout(videoLayout);
//    top->addWidget(buttonWidget);
//    layout->addWidget(nextButton);
//    top->addWidget(pauseButton);
//    top->addWidget(slider);
//    //top->addWidget(timer);
//    // showtime!

    //Window
    the_window window(videos);
 //   window.createMenu();



    //connections
    //menuBtn1->connect(menuBtn1, SIGNAL(clicked()), window,SLOT(createWidgets(the_window)));


   // the_window window(videos);
  //  window.setLayout(top);
   // window.setWindowTitle("tomeo");
   // window.setMinimumSize(800,680);



    //window.show();

    // wait for the app to terminate
//    window.setStyleSheet("background-color:black");
    window.setMinimumSize(800, 680);


    window.setStyleSheet("background-color: black;");

    return app.exec();
}
