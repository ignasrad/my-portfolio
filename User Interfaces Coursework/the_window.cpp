//
// Created by sc18ctdt on 12/12/19.
//
#include <QtMultimediaWidgets/QVideoWidget>
#include <QMediaPlaylist>
#include <fstream>
#include <QApplication>
#include "the_window.h"
#include "the_player.h"

void the_window::createMenu(){

    //Menu
   // QWidget menuWindow;
    //menuWindow.setWindowTitle("Tomeo Menu");

    //Menu Layout

/*
    QFont font = menuLbl->font();
    font.setPointSize(72);
    font.setBold(true);
    menuLbl->setFont(font);
*/

    //image
    QPixmap buttonImage0(url6);
    Icon6.addPixmap(buttonImage0);
    menuLbl->setMaximumSize(600,300);
    menuLbl->setIcon(Icon6);
    //menuLbl->setIconSize();
    menuLbl->setIconSize(QSize(600,800));


    menuBtn1->setMinimumHeight(150);
    menuBtn2->setMinimumHeight(150);
    menuBtn3->setMinimumHeight(150);
    menuBtn4->setMinimumHeight(150);
    //menu1->addWidget(menuLbl);
    menu2->addWidget(menuBtn1);
    menu2->addWidget(menuBtn2);
    menu2->addWidget(menuBtn3);
    menu2->addWidget(menuBtn4);

    menuLayout->addWidget(menuLbl);
    //menuLayout->addLayout(menu1);
    menuLayout->addLayout(menu2);
    menuLayout->setAlignment(menuLbl, Qt::AlignHCenter );

    connect(menuBtn1, SIGNAL(clicked()), this, SLOT(setLayout1()));
    connect(menuBtn2, SIGNAL(clicked()), this, SLOT(setLayout2()));
    connect(menuBtn3, SIGNAL(clicked()), this, SLOT(setLayout3()));
    connect(menuBtn4, SIGNAL(clicked()), this, SLOT(setLayout4()));

    setLayout(menuLayout);

    show();


}

void the_window::createWidgets(){



    //navigation layout scrollarea layout
    QWidget *w = new QWidget();
    QScrollArea* scroll = new QScrollArea();
    QVBoxLayout *navigationLayout = new QVBoxLayout();
    //navigation widgets
    QLabel* navigationLabel = new QLabel("Navigation Tab");
    navigationLayout->addWidget(navigationLabel);

    //set navigation button color and size
    navigationLabel ->setMinimumHeight(50);
    navigationLayout->addWidget(genre1);
    genre1 ->setMinimumHeight(50);
    navigationLayout->addWidget(genre2);
    genre2 ->setMinimumHeight(50);
    navigationLayout->addWidget(genre3);
    genre3 ->setMinimumHeight(50);
    navigationLayout->addWidget(genre4);
    genre4 ->setMinimumHeight(50);

    navigationLayout->setAlignment(Qt::AlignTop);
    //color
    navigationLabel ->setStyleSheet("color: white");
    genre1 ->setStyleSheet("background-color: #a80808");
    genre2 ->setStyleSheet("background-color: #a80808");
    genre3 ->setStyleSheet("background-color: #a80808");
    genre4 ->setStyleSheet("background-color: #a80808");
    w ->setLayout(navigationLayout);
    w ->setMinimumWidth(110);

    scroll ->setWidget(w);
    scroll ->setMaximumWidth(150);
    scroll ->setMinimumWidth(150);
    scroll ->setMinimumHeight(300);
    scroll ->setWidgetResizable(true);
    scroll ->setAlignment(Qt::AlignTop);
    scroll ->setStyleSheet("background-color: #373737");

    //video layout

    QVideoWidget *videoWidget = new QVideoWidget;
    videoLayout->addWidget(scroll);
    videoLayout->addWidget(videoWidget);
    //QMediaPlayer
    player->setVideoOutput(videoWidget);

    //buttons layout

    QWidget *buttonWidget = new QWidget();
    //next & back buttons
    QPixmap buttonImage5(url5);
    QPixmap buttonImage4(url4);

    Icon5.addPixmap ( buttonImage5, QIcon::Normal, QIcon::Off );
    Icon4.addPixmap ( buttonImage4, QIcon::Normal, QIcon::Off );

    backButton->setMaximumSize(60,60);
    backButton->setIcon(Icon4);
    backButton->setIconSize(QSize(80,60));
    backButton->setStyleSheet("background-color: #a80808");

    nextButton->setMaximumSize(60,60);
    nextButton->setIcon(Icon5);
    nextButton->setIconSize(QSize(80,60));
    nextButton->setStyleSheet("background-color: #a80808");

    //add back button to layout first
    layout->addWidget(backButton);
    buttonWidget->setLayout(layout);
    //initialize 4 video buttons
    for ( int i = 0; i < 4; i++ ) {
        TheButton *button = new TheButton(buttonWidget);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.

        buttons.push_back(button);
        layout->addWidget(button);
        button->init(&videos.at(i));

    }

    //add next button to layout
    layout->addWidget(nextButton);

    //search button image
    QPixmap buttonImage(url);
    Icon.addPixmap ( buttonImage, QIcon::Normal, QIcon::Off );
    searchButton->setIcon(Icon);
    searchButton->setIconSize(QSize(60,50));
    searchButton->setStyleSheet("background-color: #a80808");

    //setting pause button color
    pauseButton->setStyleSheet("background-color: #a80808");

    //search buton size
    search -> setMaximumWidth(300);
    search -> setMinimumHeight(50);
    searchButton->setMaximumSize(50,50);
    search->setStyleSheet("background-color: #373737; color: white;");
    h_SearchBar ->addWidget(search);
    h_SearchBar ->addWidget(searchButton);
    h_SearchBar ->setAlignment(Qt::AlignCenter);
    search ->setAlignment(Qt::AlignCenter);

    //volume slider
    slider->setRange(0,100);
    slider->setValue(0);
    slider->setTickPosition(QSlider::TicksBelow);
    slider->setTickInterval(1);
    player->setVolume(slider->value());

    //video bar
    bar->setRange(0,100);
    bar->setValue(0);
    bar->setTickPosition(QSlider::TicksBelow);
    bar->setTickInterval(1);
    bar->setMaximumWidth(600);
    bar->setMinimumHeight(50);
    bar ->setStyleSheet("QSlider::sub-page:qlineargradient { background: red;}");

    //play/pause bar
    QPixmap buttonImage2(url2);
    Icon2.addPixmap ( buttonImage2, QIcon::Normal, QIcon::Off );
    pauseButton->setMaximumSize(60,60);
    pauseButton->setIcon(Icon2);
    pauseButton->setIconSize(QSize(80,60));

    //set Hbox bar
    QHBoxLayout* h_bar = new QHBoxLayout();
    h_bar->addWidget(pauseButton);
    h_bar->addWidget(bar);
    h_bar->addWidget(time);
    h_bar->setAlignment(Qt::AlignHCenter);

    //tell player what buttons and videos are available
    player->setContent(&buttons, &videos);
    player->InitialiseVector(B);

    //set layout
    top->addLayout(h_SearchBar);
    top->addLayout(videoLayout);
    top->addLayout(h_bar);
    top->addWidget(buttonWidget);
    top->addWidget(slider);

    //setLayout(top);


    //QTimer* mTimer = new QTimer(this);
    mTimer->setInterval(100);
    mTimer->start();


    //show();
    connect(mTimer, SIGNAL(timeout()), this, SLOT(setTime()));
    connect(mTimer, SIGNAL(timeout()), this, SLOT(setBarValue()));


    //connections
    nextButton->connect(nextButton, SIGNAL(clicked()),player,SLOT(nextPage()));
    backButton->connect(backButton, SIGNAL(clicked()),player,SLOT(prevPage()));
    genre1->connect(genre1, SIGNAL(clicked()), player, SLOT(getGenre1()));
    genre2->connect(genre2, SIGNAL(clicked()), player, SLOT(getGenre2()));
    genre3->connect(genre3, SIGNAL(clicked()), player, SLOT(getGenre3()));
    search->connect(search,SIGNAL(textChanged(const QString&)), player, SLOT(setSearchText(const QString&)));
    searchButton->connect(searchButton, SIGNAL(clicked()),player,SLOT(getSearchText()));
    pauseButton->connect(pauseButton, SIGNAL(clicked()),player,SLOT(changeState()));
    pauseButton->connect(pauseButton,SIGNAL(clicked()),this,SLOT(isChecked()));
    slider->connect(slider, SIGNAL(valueChanged(int)),player,SLOT(setVolume(int)));
}

void the_window::createWidgets2(){

    delete menuLayout;
    qDeleteAll((this->children()));

    //navigation layout scrollarea layout
    QVBoxLayout *w = new QVBoxLayout();
    QScrollArea* scroll = new QScrollArea();
    QVBoxLayout *navigationLayout = new QVBoxLayout();
    //navigation widgets
    QLabel* navigationLabel = new QLabel("Navigation Tab");
    navigationLayout->addWidget(navigationLabel);
    navigationLayout->addWidget(genre1);
    navigationLayout->addWidget(genre2);
    navigationLayout->addStretch(1);
    w ->addLayout(navigationLayout);
    scroll ->setLayout(w);

    //video layout

    QVideoWidget *videoWidget = new QVideoWidget;
    videoLayout->addWidget(scroll);
    videoLayout->addWidget(videoWidget);
    //QMediaPlayer
    player->setVideoOutput(videoWidget);

    //buttons layout

    QWidget *buttonWidget = new QWidget();
    //next & back buttons
    backButton->setMinimumHeight(115);
    backButton->setMaximumWidth(80);
    nextButton->setMinimumHeight(115);
    nextButton->setMaximumWidth(80);

    //add back button to layout first
    layout->addWidget(backButton);
    buttonWidget->setLayout(layout);
    //initialize 4 video buttons
    for ( int i = 0; i < 4; i++ ) {
        TheButton *button = new TheButton(buttonWidget);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.
        buttons.push_back(button);
        layout->addWidget(button);
        button->init(&videos.at(i));
    }
    //add next button to layout
    layout->addWidget(nextButton);

    //search
    search -> setMaximumWidth(300);
    searchButton -> setMaximumWidth(60);

    //volume slider
    slider->setRange(0,100);
    slider->setValue(100);
    slider->setTickPosition(QSlider::TicksBelow);
    slider->setTickInterval(1);
    player->setVolume(slider->value());

    //video bar
    bar->setRange(0,100);
    bar->setValue(0);
    bar->setTickPosition(QSlider::TicksBelow);
    bar->setTickInterval(1);

    //tell player what buttons and videos are available
    player->setContent(&buttons, &videos);
    player->InitialiseVector(B);

    //set layout
    top->addWidget(search);
    top->addWidget(searchButton);
    top->addLayout(videoLayout);
    top->addWidget(buttonWidget);
    top->addWidget(pauseButton);
    top->addWidget(slider);
    top->addWidget(bar);
    top->addWidget(time);

    setLayout(top);


    QTimer* mTimer = new QTimer(this);
    mTimer->setInterval(100);
    mTimer->start();


    show();
    connect(mTimer, SIGNAL(timeout()), this, SLOT(setTime()));
    connect(mTimer, SIGNAL(timeout()), player, SLOT(setBarValue()));


    //connections
    nextButton->connect(nextButton, SIGNAL(clicked()),player,SLOT(nextPage()));
    backButton->connect(backButton, SIGNAL(clicked()),player,SLOT(prevPage()));
    genre1->connect(genre1, SIGNAL(clicked()), player, SLOT(getGenre1()));
    search->connect(search,SIGNAL(textChanged(const QString&)), player, SLOT(setSearchText(const QString&)));
    searchButton->connect(searchButton, SIGNAL(clicked()),player,SLOT(getSearchText()));
    pauseButton->connect(pauseButton, SIGNAL(clicked()),player,SLOT(changeState()));
    slider->connect(slider, SIGNAL(valueChanged(int)),player,SLOT(setVolume(int)));
}

void the_window::setLayout1(){

    delete menuLayout;
    qDeleteAll((this->children()));
    setLayout(top);
    slider->setValue(100);
    show();
}

void the_window::setLayout2(){

    delete menuLayout;
    qDeleteAll((this->children()));
    setLayout(top);
    player->getGenre1();
    slider->setValue(100);
    show();
}

void the_window::setLayout3(){

    delete menuLayout;
    qDeleteAll((this->children()));
    setLayout(top);
    player->getGenre2();
    slider->setValue(100);
    show();
}


void the_window::setLayout4(){

    delete menuLayout;
    qDeleteAll((this->children()));
    setLayout(top);
    player->getGenre3();
    slider->setValue(100);
    show();
}

void the_window::setTime(){
    int h,m,s;
    h = player->position()/1000/3600;
    m = (player->position()/1000-h*3600)/60;
    s = (player->position()/1000)-h*3600-m*60;
    //  QString str = QString::number(s);
    time->setText(QString("%1:%2:%3").arg(h).arg(m).arg(s));
    time->setMinimumHeight(50);
    time->setMaximumWidth(60);
}

void the_window::setBarValue(){
    int s = player->position()*100;
    int a = s/player->duration();
    bar->setValue(a);
}

void the_window::isChecked() {
    //bool ok = pauseButton->isChecked();
    //pauseButton->setChecked(false);
    pauseButton->setChecked(true);
    QPixmap buttonImage3(url3);
    Icon3.addPixmap(buttonImage3, QIcon::Normal, QIcon::Off);

    QPixmap buttonImage2(url2);
    Icon2.addPixmap(buttonImage2, QIcon::Normal, QIcon::Off);

    if (!pauseButton->isChecked()) {
        check++;
        if (check % 2 == 0) {
            pauseButton->setIcon(Icon2);
            pauseButton->setIconSize(QSize(80, 60));
        } else {
            pauseButton->setIcon(Icon3);
            pauseButton->setIconSize(QSize(80, 60));
        }
    }
}