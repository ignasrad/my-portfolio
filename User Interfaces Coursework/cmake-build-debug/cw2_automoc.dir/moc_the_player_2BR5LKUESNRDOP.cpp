/****************************************************************************
** Meta object code from reading C++ file 'the_player.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.3.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../../cserv1_a/soc_msc/sc18ctdt/Documents/cw2/the_player.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'the_player.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.3.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ThePlayer_t {
    QByteArrayData data[17];
    char stringdata[184];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ThePlayer_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ThePlayer_t qt_meta_stringdata_ThePlayer = {
    {
QT_MOC_LITERAL(0, 0, 9),
QT_MOC_LITERAL(1, 10, 15),
QT_MOC_LITERAL(2, 26, 0),
QT_MOC_LITERAL(3, 27, 16),
QT_MOC_LITERAL(4, 44, 19),
QT_MOC_LITERAL(5, 64, 2),
QT_MOC_LITERAL(6, 67, 6),
QT_MOC_LITERAL(7, 74, 14),
QT_MOC_LITERAL(8, 89, 6),
QT_MOC_LITERAL(9, 96, 8),
QT_MOC_LITERAL(10, 105, 8),
QT_MOC_LITERAL(11, 114, 11),
QT_MOC_LITERAL(12, 126, 13),
QT_MOC_LITERAL(13, 140, 13),
QT_MOC_LITERAL(14, 154, 9),
QT_MOC_LITERAL(15, 164, 9),
QT_MOC_LITERAL(16, 174, 9)
    },
    "ThePlayer\0processOneThing\0\0playStateChanged\0"
    "QMediaPlayer::State\0ms\0jumpTo\0"
    "TheButtonInfo*\0button\0nextPage\0prevPage\0"
    "changeState\0setSearchText\0getSearchText\0"
    "getGenre1\0getGenre2\0getGenre3"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ThePlayer[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   69,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    1,   70,    2, 0x08 /* Private */,
       6,    1,   73,    2, 0x0a /* Public */,
       9,    0,   76,    2, 0x0a /* Public */,
      10,    0,   77,    2, 0x0a /* Public */,
      11,    0,   78,    2, 0x0a /* Public */,
      12,    1,   79,    2, 0x0a /* Public */,
      13,    0,   82,    2, 0x0a /* Public */,
      14,    0,   83,    2, 0x0a /* Public */,
      15,    0,   84,    2, 0x0a /* Public */,
      16,    0,   85,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void ThePlayer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ThePlayer *_t = static_cast<ThePlayer *>(_o);
        switch (_id) {
        case 0: _t->processOneThing(); break;
        case 1: _t->playStateChanged((*reinterpret_cast< QMediaPlayer::State(*)>(_a[1]))); break;
        case 2: _t->jumpTo((*reinterpret_cast< TheButtonInfo*(*)>(_a[1]))); break;
        case 3: _t->nextPage(); break;
        case 4: _t->prevPage(); break;
        case 5: _t->changeState(); break;
        case 6: _t->setSearchText((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->getSearchText(); break;
        case 8: _t->getGenre1(); break;
        case 9: _t->getGenre2(); break;
        case 10: _t->getGenre3(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMediaPlayer::State >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (ThePlayer::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ThePlayer::processOneThing)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject ThePlayer::staticMetaObject = {
    { &QMediaPlayer::staticMetaObject, qt_meta_stringdata_ThePlayer.data,
      qt_meta_data_ThePlayer,  qt_static_metacall, 0, 0}
};


const QMetaObject *ThePlayer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ThePlayer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ThePlayer.stringdata))
        return static_cast<void*>(const_cast< ThePlayer*>(this));
    return QMediaPlayer::qt_metacast(_clname);
}

int ThePlayer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMediaPlayer::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void ThePlayer::processOneThing()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
