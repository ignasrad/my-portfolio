/****************************************************************************
** Meta object code from reading C++ file 'the_window.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.3.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../../cserv1_a/soc_msc/sc18ctdt/Documents/cw2/the_window.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'the_window.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.3.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_the_window_t {
    QByteArrayData data[10];
    char stringdata[101];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_the_window_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_the_window_t qt_meta_stringdata_the_window = {
    {
QT_MOC_LITERAL(0, 0, 10),
QT_MOC_LITERAL(1, 11, 14),
QT_MOC_LITERAL(2, 26, 0),
QT_MOC_LITERAL(3, 27, 7),
QT_MOC_LITERAL(4, 35, 11),
QT_MOC_LITERAL(5, 47, 10),
QT_MOC_LITERAL(6, 58, 10),
QT_MOC_LITERAL(7, 69, 10),
QT_MOC_LITERAL(8, 80, 10),
QT_MOC_LITERAL(9, 91, 9)
    },
    "the_window\0createWidgets2\0\0setTime\0"
    "setBarValue\0setLayout1\0setLayout2\0"
    "setLayout3\0setLayout4\0isChecked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_the_window[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x0a /* Public */,
       3,    0,   55,    2, 0x0a /* Public */,
       4,    0,   56,    2, 0x0a /* Public */,
       5,    0,   57,    2, 0x0a /* Public */,
       6,    0,   58,    2, 0x0a /* Public */,
       7,    0,   59,    2, 0x0a /* Public */,
       8,    0,   60,    2, 0x0a /* Public */,
       9,    0,   61,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void the_window::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        the_window *_t = static_cast<the_window *>(_o);
        switch (_id) {
        case 0: _t->createWidgets2(); break;
        case 1: _t->setTime(); break;
        case 2: _t->setBarValue(); break;
        case 3: _t->setLayout1(); break;
        case 4: _t->setLayout2(); break;
        case 5: _t->setLayout3(); break;
        case 6: _t->setLayout4(); break;
        case 7: _t->isChecked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject the_window::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_the_window.data,
      qt_meta_data_the_window,  qt_static_metacall, 0, 0}
};


const QMetaObject *the_window::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *the_window::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_the_window.stringdata))
        return static_cast<void*>(const_cast< the_window*>(this));
    return QWidget::qt_metacast(_clname);
}

int the_window::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
