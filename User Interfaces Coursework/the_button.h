//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_BUTTON_H
#define CW2_THE_BUTTON_H


#include <QtWidgets/QPushButton>
#include <vector>
#include <string>

using namespace std;
class TheButtonInfo {

public:
    QUrl* url; // video file to play
    QIcon* icon; // icon to display
    //ieskoti
    vector<string*> tags;

    TheButtonInfo ( QUrl* url, QIcon* icon, vector<string*> tags) : url (url), icon (icon), tags(tags) {}
};

class TheButton : public QPushButton {
    Q_OBJECT

public:
    TheButtonInfo* info;

     TheButton(QWidget *parent) :  QPushButton(parent) {
         setIconSize(QSize(300,110));
         connect(this, SIGNAL(released()), this, SLOT (clicked() )); // if QPushButton clicked...then run clicked() below
    }

    void init(TheButtonInfo* i);

    void nextGenre(TheButtonInfo* i);

private slots:
    void clicked();

signals:
    void jumpTo(TheButtonInfo*);

public slots:


};

#endif //CW2_THE_BUTTON_H
