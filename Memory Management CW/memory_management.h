#include <sys/types.h>

typedef struct header{
  int size;
  struct header * prev;
  struct header * next;
  int free;
  int point;
} head;

void * _malloc(size_t size);
void removeListElement(head* ptr);
int roundUp(int numToRound, int multiple);
void _free(void * ptr);
