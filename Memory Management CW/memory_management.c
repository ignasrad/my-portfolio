#include "memory_management.h"
#include <unistd.h>
#include <stdio.h>

#include <stdlib.h>


head* top = NULL;


//a function to remove an element from a linked list
void  removeListElement(head* ptr){

	if(ptr -> next == NULL){
		ptr -> prev -> next = NULL;
		return;
	}

	if(ptr == top){
		top = ptr ->next;
		top -> prev = NULL;
		return;
	}
	head *next = ptr -> next;
	head *prev = ptr -> prev;
	if(prev != NULL)
		prev-> next = next;
	if(next != NULL)
		next-> prev = prev;

}

//finction used to decide how many pages i would need when using sbrk
int roundUp(int nr, int mult)
{
    if (mult == 0)
        return nr;

    int remainder = nr % mult;
    if (remainder == 0)
        return nr;

    return nr + mult - remainder;
}

void * _malloc(size_t sizee){

	size_t size = sizee;
	//if size == 0 return null
	if(size == 0)
		return NULL;

		//padding
	while((sizeof(head) + size) % 8 != 0){
		size++;
	}

	//magic of reusing memory
	head* last = top;
	//finding the last element in the list
	while(last != NULL && last->next != NULL){
		last = last -> next;
	}

	//going through the list from bottom, so, that freeed memory would be used first
	head* this = last;
	while(this != NULL){
		//if i find a block that is free and has a size of what i need
		//just cut a slice of it
		//and leave the rest for other uses
		if(this -> free == 1 && this -> size >= size){
			if(this -> size - size > sizeof(head)){
				head* newHeader = (void*)this + sizeof(head) + size;
				newHeader->size = this->size - sizeof(head) - size;
				newHeader->free = 1;
				newHeader->next = this;
				newHeader->prev = this->prev;
				this->prev = newHeader;
				if(this->prev != NULL){
					this->prev->next = newHeader;
				}
			}
			this->free = 0;
			this->size = size;
			return (void*)this + sizeof(head);
		}
		this = this -> prev;
	}

	//if there are no suitible blocks already allocated, then lets allocate a new page
	void * block = sbrk(roundUp(sizeof(head) + size,4096));

	//taking some memory of that page for new block
	//creating the head
	head* temp = (head*)block;
	temp->size = size;
	temp->free = 0;
	if(top != NULL)
		top->prev = temp;

	temp->next = top;
	temp->prev = NULL;
	top = temp;

	//leaving the rest for other uses
	//creating a head for remaining part of memory
	head* newHeader = block + sizeof(head) + size;
	newHeader->size = 4096 - sizeof(head)*2 - size;
	newHeader->free = 1;

	newHeader->prev = NULL;
	newHeader->next = top;
	top -> prev = newHeader;
	top = newHeader;

	//returning the pointer to the start of actual memory location
	return block + sizeof(head);
}

void _free(void * ptr){

	// if ptr = null, just return
	if(ptr == NULL){
		return;
	}

	//setting given block to be free
	void *headPtr = ptr - sizeof(head);
	head *header = (head*)headPtr;
	header -> free = 1;

	//merge part muhahahahah

	//check if next block is free
	//and then merge it with current block
	if(header -> next != NULL && header -> next -> free == 1){
		head * temp = header -> next;
		temp->size += header->size + sizeof(head);
		removeListElement(header);
	}

	//check if previous block is free
	//and then merge it with current block
	if(header -> prev != NULL && header -> prev -> free == 1){
		head * temp = header -> prev;
		header->size += temp->size + sizeof(head);
		removeListElement(temp);
	}


	//return memory if a page is free
	if((void*)top + sizeof(head) == ptr){
		int memoryToFree = (top->size + sizeof(head)) / 4096;

		head * next = top->next;
		sbrk(-4096 * memoryToFree);
		top = next;
	}
}
