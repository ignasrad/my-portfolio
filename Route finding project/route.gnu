unset key
set linetype 1 linewidth 1.5 \
    pointtype 7 pointsize 0.5

set linetype 2 linewidth 1.5 \
    pointtype 7 pointsize 0.5

plot [-1.564000:-1.543100][53.801600:53.811000] 'map.out' with linespoints linestyle 5, \
                                                'route.out' with linespoints linestyle 1
