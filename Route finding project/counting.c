#include <stdio.h>
#include <float.h>

#include "counting.h"
#include "reading.h"
#include "destroy.h"


//finding fastest rout
Node* bestRoute(char filename[], int id1, int id2, int flag){


  //reading everything to thist list.
  Node* top = readNodes(filename);

  if(flag == 2)
    optimalValues(top);
  //creating pointers to beginning and ending points
  Node* a = getPointer(id1, top);
  Node* b = getPointer(id2, top);


  //chencking if given points exist
  if(a == NULL){
    printf("Node id %i does not exist!\n", id1);
    return NULL;
  }

  if(b == NULL){
    printf("Node id %i does not exist!\n", id2);
    return NULL;
  }

  //setting a value to 0 because it is the fisrst point of the route
  //then counting values for the nodes that are connected to a
  a->value = 0;
  valueCount(a);

  //creating a new list that has only those nodes,  which values are less
  //than DBL_MAX
  Node* simpleGraph;

  //adding a to that new list
  if(top->next == NULL)
    printf("List has only one node\n");
  else{
    if(a == top){
      simpleGraph = top;
      top = top->next;
    }
    else
      simpleGraph = removeNode(a, top);
  }
  simpleGraph -> next = NULL;

  Node* this;

  //next node to concern is the one with the lowest value
  if(lowestValue(top) != NULL){
    this = lowestValue(top);
  }
  else{
    printf("Given nodes are not connected  in any way!\n");
    return NULL;
  }

  //repeating everything that was done with (a) many times before reaching (b)
  while(this != b){

    valueCount(this);
    if(this == top){
      top = top->next;
    }
    else
      removeNode(this, top);

    Node* temp;
    temp = simpleGraph;

    simpleGraph = this;
    simpleGraph -> next = temp;

    if(lowestValue(top) != NULL){
      this = lowestValue(top);
    }
    else{
      printf("Given nodes are not connected  in any way!\n");
      return NULL;
    }
  }



  //adding (b) to the simplified list
  if(this == b){

    if(this == top){
      top = top->next;
    }
    else
      removeNode(this, top);

    Node* temp;
    temp = simpleGraph;

    simpleGraph = this;
    simpleGraph -> next = temp;
  }

  destroyList(top); //destroying not used nodes
  return finalRoute(simpleGraph,a);
}

// removes node from the list
// !!!!   does not free the memory !!!!!
// suited for moving element to another list
Node* removeNode(Node* x, Node* top){

  Node* this;
  this = top;

  if(top == x){
    return x;
  }


  while(this != NULL){
    if(this -> next == x){
      this -> next = x->next;
      return x;
    }
    this = this -> next;
  }
  return NULL;
}


//creates a route from the simpliefied graph
Node* finalRoute(Node* top, Node* a){

  Node* route;
  route = top;

  top = route->next;

  route->next = NULL;

  while(route != NULL && route != a){

    Node* lowest;
    lowest = route->conTop->node;

    ConNode* this;
    this = route->conTop;
    while(this != NULL){
      if(this->node->value <= lowest->value){
        lowest = this->node;
      }
      this = this->next;
    }

    if(lowest == top){
      top = top->next;
    }
    else
      removeNode(lowest, top);

    Node* tempTop;
    tempTop = route;

    route = lowest;
    route -> next = tempTop;
  }
  destroyList(top);
  return route;
}

void optimalValues(Node* top){


   Node* this = top;

   while(this != NULL){

     ConNode* temp = this -> conTop;
     while (temp != NULL) {
        if(temp->length >= 0 && temp->veg >= 1)
          temp->optimal = temp -> length / (temp-> veg * 100);
        else if(temp->veg == 1 && temp->length == 1){
          //if one edge has veg = 1 and length = 1
          //and other has veg = 0 and length = 1
          //they would have been teated the same, while
          //the one with veg 1 is greener
          //this prevents it from happening
          temp->optimal += 0.001;
        }
       temp = temp -> next;
     }
     this = this -> next;
   }
}
//finds lowest value in the list
Node* lowestValue(Node* top){
  Node* node;
  node = top;

  Node* lowest;
  lowest = node;

  double value = DBL_MAX;

  while(node != NULL){

    if(node -> value < value){
      lowest = node;
      value = node -> value;
    }
    node = node ->next;
  }

  if(lowest->value == DBL_MAX)
    return NULL;
  else
    return lowest;
}


//counts the value of all connected vertices
void valueCount(Node* current){

  ConNode* this;
  this = current->conTop;
  while(this != NULL){
    if(current->value + this->optimal < this->node->value){
      this->node->value = current->value + this->optimal;
    }
    this = this -> next;
  }
}

//gets ponter of node given its id
Node* getPointer(int id, Node* top){

  Node* this;
  this = top;
  while(this != NULL){
    if(this -> id == id){
      return this;
    }
    this = this -> next;
  }

  return NULL;
}

//shouts out the size of given list
//used while testing
int listSize(Node* top){
  int i=0;

  Node* this = top;
  while(this != NULL){
    if(i>4)
      printf("this : %i   this->next :   %i\n", this->id, this->next->id);
    i++;
    this = this ->next;
  }

  return i;
}
