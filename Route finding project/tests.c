#include  <stdio.h>
#include  <stdbool.h>

#include "reading.h"
#include "writing.h"
#include "destroy.h"
#include "counting.h"
#include "unity.h"

bool compareLists(Node* x, Node* y);
//testing reading module
void test_makeNode()
{
  // id testing
  TEST_ASSERT_EQUAL_INT(5,makeNode(5,1.1,0.0)->id);
  TEST_ASSERT_EQUAL_INT(-5,makeNode(-5,1.1,0.0)->id);
  TEST_ASSERT_EQUAL_INT(0,makeNode(0,1.1,0.0)->id);
  TEST_ASSERT_EQUAL_INT(99999999,makeNode(99999999,1.1,0.0)->id);
  TEST_ASSERT_EQUAL_INT(-99999999,makeNode(-99999999,1.1,0.0)->id);

  // latitude testing
  TEST_ASSERT_EQUAL_FLOAT(1.1,makeNode(5,1.1,0.0)->lat);
  TEST_ASSERT_EQUAL_FLOAT(-1.1,makeNode(5,-1.1,0.0)->lat);
  TEST_ASSERT_EQUAL_FLOAT(9999.1,makeNode(5,9999.1,0.0)->lat);
  TEST_ASSERT_EQUAL_FLOAT(0,makeNode(5,0,0.0)->lat);

  // longtitue testing
  TEST_ASSERT_EQUAL_FLOAT(0,makeNode(5,1.1,0.0)->lon);
  TEST_ASSERT_EQUAL_FLOAT(9.9,makeNode(5,0,9.9)->lon);
  TEST_ASSERT_EQUAL_FLOAT(-9.9,makeNode(5,9999.1,-9.9)->lon);
  TEST_ASSERT_EQUAL_FLOAT(0,makeNode(5,0,0.0)->lon);
}

void test_addLink(void)
{
  //creating simple list
  Node* top = makeNode(0,0,0);
  top->next = makeNode(1,2,2);
  top->next->next = makeNode(2,3,3);

  Node* node1 = top->next;
  Node* node2 = node1->next;

  addLink(top, 0, 2, 888, 9.9);
  TEST_ASSERT_EQUAL_INT_MESSAGE(2,top->conTop->node->id, "Testing id");
  TEST_ASSERT_EQUAL_INT_MESSAGE(0,node2->conTop->node->id, "Testing id");

  TEST_ASSERT_EQUAL_FLOAT_MESSAGE(888,top->conTop->length, "Conection length");
  TEST_ASSERT_EQUAL_FLOAT_MESSAGE(888,node2->conTop->length, "Conection length");

  TEST_ASSERT_EQUAL_FLOAT_MESSAGE(9.9,top->conTop->veg, "Conection vegitation");
  TEST_ASSERT_EQUAL_FLOAT_MESSAGE(9.9,node2->conTop->veg, "Conection vegitation");
}

void test_readNodes(void)
{
  Node* top = readNodes("smalltest.map");
  Node* bottom = top->next->next->next->next;
  TEST_ASSERT_EQUAL_INT_MESSAGE(112,top->id, "Testing top id");
  TEST_ASSERT_EQUAL_INT_MESSAGE(13,bottom->id, "Testing bottom id");

  TEST_ASSERT_EQUAL_FLOAT_MESSAGE(53.817941, bottom->lat, "Testing top latitude");
  TEST_ASSERT_EQUAL_FLOAT_MESSAGE(53.810000, top->lat, "Testing bottom latitude");


  TEST_ASSERT_EQUAL_INT_MESSAGE(12, top->conTop->node->id, "Testing top con id");
  TEST_ASSERT_EQUAL_INT_MESSAGE(13, top->next->conTop->node->id, "Testing top->next con id");


}

//testing writing module

void test_writingNode(void)
{

}

void test_writingRoute(void)
{

}

//testing fastets path finding module
void test_getPointer(void)
{

  Node* top = readNodes("smalltest.map");
  TEST_ASSERT_EQUAL_INT(12, getPointer(12,top)->id);
  TEST_ASSERT_EQUAL_INT(112, getPointer(112,top)->id);
  TEST_ASSERT_EQUAL_INT(10, getPointer(10,top)->id);

  TEST_ASSERT_NULL(getPointer(100,top));
}

void test_valueCount(void)
{
  Node* top = readNodes("smalltest.map");

  Node* n10 = getPointer(10, top);
  Node* n11 = getPointer(11, top);
  Node* n12 = getPointer(12, top);
  Node* n13 = getPointer(13, top);
  Node* n112 = getPointer(112, top);


  n10->value = 0;
  valueCount(n10);

  // value count for node id 10
  TEST_ASSERT_EQUAL_FLOAT_MESSAGE(5.315219, n11->value, "value count for 10");
  TEST_ASSERT_EQUAL_FLOAT_MESSAGE(20.719777, n12->value, "value count for 10");
  TEST_ASSERT_EQUAL_FLOAT_MESSAGE(1.719777, n13->value, "value count for 10");


  // value count for 11
  valueCount(n11);
  TEST_ASSERT_EQUAL_FLOAT_MESSAGE(0, n10->value, "value count for 11");
  TEST_ASSERT_EQUAL_FLOAT_MESSAGE(7.940679, n12->value, "value count for 11");
  TEST_ASSERT_EQUAL_FLOAT_MESSAGE(12.034996, n112->value, "value count for 11");
  TEST_ASSERT_EQUAL_FLOAT_MESSAGE(1.719777, n13->value, "value count for 11");
}

void test_lowestValue(void)
{
  //creating a simple list
  Node* top = makeNode(0,0,0);
  top->next = makeNode(1,2,2);
  top->next->next = makeNode(2,3,3);

  //seting top value to the lowest
  top->value = 10;

  TEST_ASSERT_EQUAL_INT_MESSAGE(0, lowestValue(top)->id, "Top has the lowest");

  //making second node(node id 1) to have lowest value
  top->next->value = 9;
  TEST_ASSERT_EQUAL_INT_MESSAGE(1, lowestValue(top)->id, "Second has the lowest");

  //making second node(node id 1) to have lowest value
  top->next->next->value = 8;
  TEST_ASSERT_EQUAL_INT_MESSAGE(2, lowestValue(top)->id, "Last has the lowest");
}

void test_removeNode(void)
{
  Node* top = readNodes("smalltest.map");

  Node* n10 = getPointer(10, top);
  Node* n11 = getPointer(11, top);
  Node* n12 = getPointer(12, top);
  Node* n13 = getPointer(13, top);
  Node* n112 = getPointer(112, top);

  //removing bottom
  removeNode(n13, top);
  TEST_ASSERT_NULL_MESSAGE(getPointer(13,top), "Bottom is not removed!");

  //removing from the middle
  removeNode(n12, top);
  TEST_ASSERT_NULL_MESSAGE(getPointer(12,top), "Bottom is not removed!");

  //trying to remove a node that is not in the list
  TEST_ASSERT_NULL_MESSAGE(removeNode(n13, top), "trying to remove a node that is not in the list!");
}

void test_bestRoute(void)
{
  Node* top = readNodes("smalltest.map");

  //making first route
  Node* route1 = getPointer(10, top);
  route1->next = getPointer(11, top);
  route1->next->next = getPointer(112, top);
  route1->next->next->next = NULL;

  TEST_ASSERT_TRUE(compareLists(route1, bestRoute("smalltest.map", 10, 112, 1)));

  top = readNodes("smalltest1.map");
  //making another route
  Node* route2 = getPointer(1, top);
  route2->next = getPointer(3, top);
  route2->next->next = getPointer(5, top);
  route2->next->next->next = NULL;

  TEST_ASSERT_TRUE(compareLists(route2, bestRoute("smalltest1.map", 1, 5,1)));

  //checking for changed parameters
  char filename[100] = "Final_Map.map";

  int a = 26653393;
  int b = -2452;


  top = readNodes(filename);
  Node* start = getPointer(a, top);
  Node* end = getPointer(b,top);

  Node* route = bestRoute(filename, a, b, 2);
  TEST_ASSERT_TRUE_MESSAGE(route->id == start->id, "Start id changed");
  TEST_ASSERT_TRUE_MESSAGE(route->lat == start->lat, "Start latitude changed");
  TEST_ASSERT_TRUE_MESSAGE(route->lon == start->lon, "Start longtitude changed");

  TEST_ASSERT_TRUE_MESSAGE(getPointer(b, route)->id == end->id, "End id changed");
  TEST_ASSERT_TRUE_MESSAGE(getPointer(b, route)->lat == end->lat, "End latitude changed");
  TEST_ASSERT_TRUE_MESSAGE(getPointer(b, route)->lon == end->lon, "End longtitude changed");
}

//function to compare two linked lists
bool compareLists(Node* x, Node* y){

  Node* a = x;
  Node* b = y;

  while(a != NULL){
    if(b == NULL){
      return false;
    }

    if(a->id != b->id){
      return false;
    }
    a = a -> next;
    b = b -> next;
  }

  if(a == NULL && b == NULL)
    return true;
  else
    return false;
}

int main ( void )
{
    UNITY_BEGIN();

    RUN_TEST ( test_makeNode );
    RUN_TEST ( test_addLink );
    RUN_TEST ( test_readNodes );
    RUN_TEST ( test_getPointer );
    RUN_TEST ( test_valueCount );
    RUN_TEST ( test_lowestValue );
    RUN_TEST ( test_removeNode );
    RUN_TEST ( test_bestRoute );

    return UNITY_END();
}
