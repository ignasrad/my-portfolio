#include <stdlib.h>
#include <stdio.h>
#include <float.h>

#include "reading.h"
#include "dataStruct.h"
#include "writing.h"


//reads bordedrs and calls function to write them to gnu parameters file
void readBorder(FILE* fr){
  double minLat, minLon, maxLat, maxLon;


  char line[256];
  char temp[10];
  if(fgets(line, sizeof(line), fr) && line[0]=='<' && line[1]=='b' && line[2]=='o'){
    sscanf(line, "<bounding minLat=%lf minLon=%lf maxLat=%lf maxLon=%lf /bounding>\n", &minLat, &minLon, &maxLat, &maxLon);
  }
  else printf("First line is not bounding!!!!\n");
  writingParameters(minLat,minLon,maxLat,maxLon);
}

// goes through the file two times, firtst reads the nodes
// then links them tohether
Node* readNodes(char filename[]){

  FILE* fr = fopen(filename,"r");

  Node* current;
  Node* top;
  top = NULL;
  current = top;
  char line[256];

  while (fgets(line, sizeof(line), fr)) {

    int id;
    double lat, lon;
    if(line[0] == '<' && line[1] == 'n' && line[2] == 'o' && line[3] == 'd' && line[4] == 'e'){
      parseNode(line, &id, &lat, &lon);

      if(current == NULL){
        current = makeNode(id,lat,lon);
        top = current;
      }
      else{
        current->next = makeNode(id,lat,lon);
        current = current -> next;
      }
    }
  }
  
  //starting from the beginning and searching for links;
  fseek(fr, 0, SEEK_SET);

  while (fgets(line, sizeof(line), fr)) {
    if(line[0] == '<' && line[1] == 'l' && line[2] == 'i' && line[3] == 'n' && line[4] == 'k'){
      int id,node1,node2;
      double length, veg;
      parseLink(line, &id, &node1, &node2, &length, &veg);
      addLink(top,node1,node2, length, veg);
    }
  }

  fclose(fr);
  return top;
}


//Parses node to id lat and lon, takes a string and pointers to destinations of each argument
void parseNode(char str[], int* ID, double* LAT, double* LON){
  char id[100];
  char lat[100];
  char lon[100];

  int i=0;
  while(str[i] != '\0'){
    if(str[i] == 'i' && str[i+1] == 'd' && str[i+2] == '='){
      i+=3;

      //parsing ID
      int j=0;
      while(str[i] != ' '){
        id[j] = str[i];
        ++i;
        ++j;
      }
      id[j]= '\0';

      //parsing latitude
      i+=5;
      j=0;
      while(str[i] != ' '){
        lat[j] = str[i];
        ++i;
        ++j;
      }
      lat[j]= '\0';

      //parsing longtitude
      i+=5;
      j=0;
      while(str[i] != ' '){
        lon[j] = str[i];
        ++i;
        ++j;
      }
      lon[j]= '\0';
      break;
    }
    ++i;
  }

  sscanf(id, "%i", ID);
  sscanf(lat, "%lf", LAT);
  sscanf(lon, "%lf", LON);
}

//adds links to the nodes;
void addLink(Node* top, int node1, int node2, double length, double v){

  Node* this;
  Node* first;
  Node* second;
  this = top;
  int connectedTo=0;

  if(top == NULL){
    return;
  }

  while(this != NULL && connectedTo < 3){

    if(this -> id == node1){
      first = this;
      ++connectedTo;
    }

    if(this -> id == node2){
      second = this;
      ++connectedTo;
    }
    this = this->next;
  }
  addConnectedNode(first,second,length, v);
  addConnectedNode(second,first,length, v);
}

void addConnectedNode(Node* node1, Node* node2, double length, double veg){

  ConNode *temp = (ConNode *)malloc(sizeof(ConNode));

  temp->veg = veg;
  temp->length = length;
  temp->optimal = length;
  temp->node = node2;
  temp->next = node1->conTop;

  node1->conTop = temp;
}

void parseLink(char str[], int* ID, int* node1, int* node2, double* length, double* v){
  double arch, land;
  int way;

  sscanf(str, "<link id=%i node=%i node=%i way=%i length=%lf veg=%lf arch=%lf land=%lf POI=",
    ID, node1, node2, &way, length, v, &arch, &land);
}

//makes a node
Node* makeNode(int id, double lat, double lon){

  Node *node = (Node *)malloc(sizeof(Node));

  node-> next = NULL;
  node-> conTop = NULL;
  node-> value = DBL_MAX;

  node-> lat = lat;
  node-> lon = lon;
  node-> id = id;

  return node;
}
