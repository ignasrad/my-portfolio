#ifndef _reading
#define _reading

#include "dataStruct.h"
void readBorder(FILE* fr);
Node* readNodes(char filename[]);
void parseNode(char str[], int* ID, double* LAT, double* LON);

Node* makeNode(int id, double lat, double lon);
void addLink(Node* top, int node1, int node2, double length, double v);
void addConnectedNode(Node* node1, Node* node2, double length, double veg);
void parseLink(char str[], int* ID, int* node1, int* node2, double* length, double* v);
#endif
