cmake_minimum_required (VERSION 2.6)
project (Quadtree)
set(CMAKE_C_FLAGS "-std=c99 -lm")



add_executable(
  route
  Route.c
  reading.c
  destroy.c
  writing.c
  counting.c
)
