#ifndef _counting
#define _counting
#include "dataStruct.h"

void valueCount(Node* current);
Node* getPointer(int id, Node* top);
Node* bestRoute(char filename[], int id1, int id2, int flag);
Node* removeNode(Node* x, Node* top);
Node* lowestValue(Node* top);
Node* finalRoute(Node* top, Node* id);
void optimalValues(Node* top);

int listSize(Node* top);
#endif
