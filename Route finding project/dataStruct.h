#ifndef _struct
#define _struct

struct connected{
  struct node* node;
  double length;
  double veg;
  double optimal;
  struct connected* next;
};
typedef struct connected ConNode;

struct node{
  int id;
  double lon, lat;
  double value;
  struct node* next;
  struct connected* conTop; //pointer to the top of connected verttices stack
};
typedef struct node Node;

#endif
