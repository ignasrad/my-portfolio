#include <stdlib.h>
#include <stdio.h>
#include "dataStruct.h"
#include "writing.h"

void writingNode(Node* top){
  FILE *fp = fopen("map.out", "w");

  Node* current;
  current = top;

  while (current != NULL) {

      ConNode* temp;
      temp = current->conTop;
      while(temp != NULL){
        fprintf(fp, "%lf %lf\n", current->lon, current->lat);
        fprintf(fp, "%lf %lf\n\n", temp->node->lon, temp->node->lat);
        temp = temp -> next;
      }
    current = current->next;
  }

  fclose(fp);
}

void writingRoute(Node* top){

  FILE *fp = fopen("route.out", "w");

  Node* current;
  current = top;
  if(top == NULL){
    fprintf(fp, "%lf %lf\n", 0.0, 0.0);
    fclose(fp);
    return;
  }

  while (current->next != NULL) {
    fprintf(fp, "%lf %lf\n", current->lon, current->lat);
    fprintf(fp, "%lf %lf\n\n", current->next->lon, current->next->lat);
    current = current->next;
  }

  fclose(fp);
}

void writingParameters(double minLat, double minLon, double maxLat, double maxLon){
    FILE *par = fopen("map.gnu", "w");
    fprintf(par, "unset key\n");
    fprintf(par, "plot [%lf:%lf][%lf:%lf]", minLon, maxLon, minLat, maxLat);
    fprintf(par, " 'map.out' using 1:2 pt 7 ps 1  \n");
    fclose(par);
}
