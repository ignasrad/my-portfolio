#ifndef _writing
#define _writing

#include "dataStruct.h"
void writingRoute(Node* top);
void writingNode(Node* top);
void writingParameters(double minLat, double minLon, double maxLat, double maxLon);
#endif
