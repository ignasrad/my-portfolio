#include <stdlib.h>
#include <stdio.h>
#include <float.h>

#include "destroy.h"


void destroyList(Node* top){

  Node* current;
  while(top != NULL){

    //unconecting top from others.
    ConNode* this = top -> conTop;

    while(this != NULL){

      Node* node = this->node;
      ConNode* temp = node -> conTop;
      if(temp == NULL){
        this = this->next;
        continue;
      }
      if(temp->node == top){
        node -> conTop = node -> conTop -> next;
        free(temp);
      }
      else{
        while(temp->next->node != top ){
          temp = temp->next;
        }
        ConNode* toFree = temp->next;
        temp->next = temp->next->next;
        free(toFree);
      }


      this= this->next;
    }
    //unconnecting ends
    destroyConnectedList(top->conTop);
    current = top;
    top = top ->next;
    free(current);
  }
}

void destroyConnectedList(ConNode* top){
  ConNode* current;

  while(top != NULL){
    current = top;
    top = top -> next;
    free(current);
  }
}
