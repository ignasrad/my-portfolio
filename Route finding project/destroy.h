#ifndef _destroying
#define _destroying

#include "dataStruct.h"

void destroyList(Node* top);
void destroyConnectedList(ConNode* top);

#endif
