#include  <stdio.h>

#include "reading.h"
#include "writing.h"
#include "destroy.h"
#include "counting.h"

int main(int argc, char const *argv[]) {

  char filename[100] = "Final_Map.map";

  int a = 31004229;
  int b = 21133760;


  Node* mapTop = readNodes(filename);

  int choice = 3;



  printf("Welcome to route finding app.\n");
  printf("CHOOSE ONE FROM BELOW:.\n");
  while(choice != 1 && choice != 2){
    printf("1.    The shortest route.\n");
    printf("2.    The green route.\n");
    scanf("%i", &choice);
  }


  printf("Enter the first node id\n");
  scanf("%i", &a);
  while(getPointer(a, mapTop) == NULL){
    printf("Node %i does not exist\n", a);
    printf("Enter the first node id\n");
    scanf("%i", &a);
  }

  printf("Enter the second node id\n");
  scanf("%i", &b);
  while( getPointer(b, mapTop) == NULL){
    printf("Node %i does not exist\n", b);
    printf("Enter the second node id\n");
    scanf("%i", &b);
  }



  Node* route = bestRoute(filename,a, b, choice);


  writingNode(mapTop);
  writingRoute(route);
  destroyList(route);
  destroyList(mapTop);
  return 0;
}
