src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"

$(document).ready(function(){


  if(localStorage.hasOwnProperty('name')){
    $("#welcome").text("Welcome back, " + localStorage.getItem('name') + "!");
    $("#welcome").show();
    $("#form").hide();
  }

  $("#submit").click(function(){
    // var name = $('#name').text();
    localStorage.setItem("name", $('#name').val());
    localStorage.setItem("brand", $('#brand').val());
    alert("Thank you");
    $("#form").hide();
    $("#welcome").text("Welcome, " + localStorage.getItem('name') + "!");
    $("#welcome").show();
  });


});

// google map
function initMap() {
  // The location of Uluru
  var location = {lat: 53.800755, lng: -1.549077};

  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 5, center: location});

      var bikePaths = new google.maps.BicyclingLayer();
      bikePaths.setMap(map);


    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
        map.setCenter(pos);
        map.setZoom(10);
      });
    }

}

// car production visualisation
function popup(){
    document.getElementById("popup").classList.toggle("show");


    function printCounter() {
        var icons = ["<img src ='Cars/i1.svg'> ","<img src ='Cars/i2.svg'>","<img src ='Cars/i3.svg'>","<img src ='Cars/truck.svg'>"];

        var x= document.getElementById("popup");

        var counter = 0;

        x.innerHTML = x.innerHTML + icons[Math.floor(Math.random() * 4)];
        if (!document.getElementById("popup").classList.contains('show')) {
          x.innerHTML = "<h1> A new car enters streets every 0.45 sec ! </h1><hr />";
          counter = 1000;
        }

        counter++;
        if (counter < 1000){
            setTimeout(printCounter, 450);
        }
    }

    // Start the loop
    printCounter();

}


// mobile navigation
function demo() {

    $("#dropCon").slideToggle("slow");
}

window.onclick = function(event) {
  if (!event.target.matches('.dropDown')) {
    var dropdowns = document.getElementsByClassName("dropdownContainer");
    var i;
    for (i = 0; i < dropdowns.length; i++) {

        $("#dropCon").slideUp();

    }
  }
  if (!event.target.matches('.popBut')) {

    var pops = document.getElementsByClassName("pop");
    var i;
    for (i = 0; i < pops.length; i++) {
      var openPop = pops[i];

      if (openPop.classList.contains('show')) {
        openPop.classList.remove('show');
      }
    }
  }
}
