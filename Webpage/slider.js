//slider
var slideIndex = 1;

  showSlide();


function showSlide(){
  var i;

  var slides  = document.getElementsByClassName('slide');
  if(slides.length <= slideIndex){
    slideIndex = 1;
  }
  if(slideIndex < 1){
    slideIndex = slides.length-1;
  }

  for(i=0; i<slides.length; i++){
    if(slides[i].classList.contains('show')){
      slides[i].classList.remove('show');
    }
  }
  if(slides.length > 0)
    slides[slideIndex].classList.toggle('show');
}

function changeSlide(n){
  slideIndex+= n;
  showSlide();
}
