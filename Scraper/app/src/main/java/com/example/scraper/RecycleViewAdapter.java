package com.example.scraper;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.ViewHolder>{

    private ArrayList<Car> cars;
    private Context context;

    public RecycleViewAdapter(Context context, CarList cars) {
        this.cars = cars.getCars();
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
       Car car = cars.get(position);
       String title = car.getTitle();

       //limiting length of the title to 53 chars
       if (title.length() > 50){
           title = title.substring(0,47) + "...";
       }

       holder.image.setImageBitmap(car.getBitmapImg());
       holder.price.setText(car.getPrice() + "€");
       holder.platform.setText(title);

        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setCornerRadius(80f);

       if(car.getLink().contains("autoplius"))
           gradientDrawable.setColor(Color.parseColor("#007bbc"));

       if(car.getLink().contains("autogidas"))
           gradientDrawable.setColor(Color.parseColor("#f08c1c"));

        if(car.getLink().contains("autobilis"))
            gradientDrawable.setColor(Color.parseColor("#df1d38"));


       holder.parentLayout.setBackground(gradientDrawable);

       holder.parentLayout.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(cars.get(position).getLink()));
                context.startActivity(intent);
           }                      
       });
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView image;
        TextView price;
        TextView platform;
        RelativeLayout parentLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            price = itemView.findViewById(R.id.carPrice);
            platform = itemView.findViewById(R.id.platform);
            image = itemView.findViewById(R.id.image);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }
}
