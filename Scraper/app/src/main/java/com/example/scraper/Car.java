package com.example.scraper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Car {

    private int price;
    private String link;
    private boolean activeness;
    private int evaluation;
    private Date date;
    private String imgLink;
    private Bitmap bitmapImg;
    private String title;

    public Car(){
        price = 0;
        link = null;
        activeness = false;
        evaluation = 0;
        date = new Date();
        bitmapImg = null;
    }

    public Car(int givenPrice, String givenLink, String givenImgLink, String givenTitle){
        price = givenPrice;
        link = givenLink;
        imgLink = givenImgLink;
        date = new Date();
        bitmapImg = null;
        title = givenTitle;
    }


    public int getPrice(){
        return price;
    }

    public String getLink(){
        return link;
    }

    public boolean isActive(){
        return activeness;
    }

    public int getEvaluation(){
        return evaluation;
    }

    public Date getDate(){
        return date;
    }

    public String getTitle(){
        return title;
    }

    public void setActiveness(boolean state){
        activeness = state;
    }

    private void setBitmapImg(Bitmap img){
        bitmapImg = img;
    }

    public Bitmap getBitmapImg(){
        downloadBitmapImg();
        if(bitmapImg == null)
            throw new NullPointerException("Trying to return null bitmap of an image");
        else return bitmapImg;
    }

    public void parseLine(String line){

        String[] temp = line.split(",");

        if(temp.length < 7){
            throw new ArrayIndexOutOfBoundsException("Invalid format of the given line");
        }

        title = temp[0];
        link = temp[1];
        price = Integer.parseInt(temp[3]);
        evaluation = Integer.parseInt(temp[5]);

        if(temp[4] == "true")
            activeness = true;
        else
            activeness = false;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = formatter.parse(temp[2]);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        imgLink = temp[6];
    }

    public void downloadBitmapImg(){

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    InputStream is = (InputStream) new URL(imgLink).getContent();
                    Bitmap d = BitmapFactory.decodeStream(is);
                    is.close();
                    setBitmapImg(d);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        t.start();

        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean isInGivenList(CarList cars){
        ArrayList<Car> list = cars.getCars();

        for(Car el : list){
            if(el.getLink().equals(link)){
                return true;
            }
        }

        return false;
    }

    @Override
    public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return title.replace(',','/') + ',' + link + ',' + formatter.format(date) + ',' + Integer.toString(price) + ',' + Boolean.toString(activeness) + ',' + Integer.toString(evaluation) + ',' + imgLink +'\n';
    }
}
