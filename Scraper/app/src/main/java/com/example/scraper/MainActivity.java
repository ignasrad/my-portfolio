package com.example.scraper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private CarList carsFromCsv = new CarList();
    private CarList activeCars = new CarList();
    private CarList newCars = new CarList();
    private final String csvFileName = "testas.csv";

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        execute();

        //drawer menu
        drawerLayout = findViewById(R.id.drawerLayout);
        toggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open,R.string.close);
        toolbar = findViewById(R.id.nav_action);
        toolbar.setTitle("NEW CARS");
        setSupportActionBar(toolbar);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navView = findViewById(R.id.nav_view);
        navView.setNavigationItemSelectedListener(this);
        navView.setCheckedItem(R.id.nav_latest);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                new LatestFragment(newCars)).commit();

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()){
            case R.id.nav_latest:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new LatestFragment(newCars)).commit();
                toolbar.setTitle("NEW CARS");
                 break;
            case R.id.nav_active:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new ActiveFragment(activeCars)).commit();
                toolbar.setTitle("ACTIVE CARS");
                break;
            case R.id.nav_share:

                final File file = this.getFileStreamPath(csvFileName);

                try {
                    File tempFile = File.createTempFile("data",".csv", getExternalCacheDir());

                    FileWriter fw = new FileWriter(tempFile);

                    FileReader fr = new FileReader(file);
                    int c = fr.read();
                    while (c != -1) {
                        fw.write(c);
                        c = fr.read();
                    }
                    fr.close();

                    fw.flush();
                    fw.close();

                final Uri data = FileProvider.getUriForFile(this,
                        getApplicationContext().getPackageName() + ".myprovider", file);
                this.grantUriPermission(this.getPackageName(), data, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                final Intent intent = new Intent(Intent.ACTION_SEND)
                        .setDataAndType(data, "text/csv")
                        .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                        .putExtra(Intent.EXTRA_STREAM, Uri.fromFile(tempFile))
                        .putExtra(Intent.EXTRA_SUBJECT, "data");
                this.startActivity(Intent.createChooser(intent, "send"));


                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void execute(){

        //scraping sites
        activeCars.scrapeAutoPlius();
        activeCars.scrapeAutoGidas();
        activeCars.scrapeAutoBilis();
        activeCars.priceIncreasingOrder();

        //reading csv
        carsFromCsv.readCsv(csvFileName, this);
        carsFromCsv.priceIncreasingOrder();

        //filtering out the new cars
        newCars = activeCars.getUniqueCars(carsFromCsv);

        addNewToCsv();
        carsFromCsv.writeToCsv(this);

    }

    private void addNewToCsv(){

        ArrayList<Car> newOnes = newCars.getCars();

        for(Car n : newOnes){
            carsFromCsv.addCar(n);
        }
    }

}
