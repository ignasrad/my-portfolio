package com.example.scraper;

import android.content.Context;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;

public class CarList {

    private ArrayList<Car> cars;

    public CarList() {
        cars = new ArrayList<Car>();
    }

    public ArrayList<Car> getCars() {
        return cars;
    }

    public String readCsv(String fileName, Context context) {
        File file = context.getFileStreamPath(fileName);
        System.out.println(file.getAbsolutePath());
        String content = "";
        if (file.exists()) {
            try {
                InputStream in = context.openFileInput(fileName);
                if (in != null) {
                    InputStreamReader tmp = new InputStreamReader(in);
                    BufferedReader reader = new BufferedReader(tmp);
                    String str;
                    StringBuilder buf = new StringBuilder();
                    while ((str = reader.readLine()) != null) {
                        buf.append(str + "\n");
                    }
                    in.close();
                    content = buf.toString();
                }
            } catch (java.io.FileNotFoundException e) {
            } catch (Throwable t) {
                Toast.makeText(context, "Exception: " + t.toString(), Toast.LENGTH_LONG).show();
            }

            String[] lines = content.split("\n");

            cars.clear();

            for (String i : lines) {
                Car temp = new Car();
                try {
                    temp.parseLine(i);
                    cars.add(temp);
                }
                catch (ArrayIndexOutOfBoundsException e){
                    System.out.println("Invalid line!" + i);
                }

            }


        }
        return content;
    }

    public void scrapeAutoPlius() {

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                Elements els;
                try {
                    Document doc = Jsoup.connect("https://autoplius.lt/skelbimai/naudoti-automobiliai?slist=880399255&category_id=2&make_date_to=1990&make_id=43&model_id=202").get();
                    els = doc.getElementsByClass("announcement-item");
                    parseAutoPlius(els);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void parseAutoPlius(Elements els) {
        if (els != null) {
            for (Element element : els) {
                Elements img = element.getElementsByAttribute("src");
                Elements pricing = element.getElementsByClass("announcement-pricing-info");
                String price = pricing.first()
                        .text()
                        .replace('€',' ')
                        .replaceAll("\\s","");

                String title = element.getElementsByClass("announcement-title").first().text();
                String imgLink;

                if(img.first() == null){
                    imgLink = "https://autogidas.lt/static/images/b_nofoto.gif";
                }
                else
                    imgLink = img.first().attr("src");

                Car temp = new Car(Integer.parseInt(price.split("[+/]+")[0]), element.attr("href"), imgLink, title);
                cars.add(temp);
            }
        }
    }

    public void scrapeAutoBilis(){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                Elements els;
                try {
                    Document doc = Jsoup.connect("https://www.autobilis.lt/skelbimai/naudoti-automobiliai?order_by=created_at-desc&category_id=1&make_id%5B%5D=243&model_id%5B%5D=8589&year_to=1990").get();
                    els = doc.getElementsByClass("search-rezult-content");
                    parseAutoBilis(els);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void parseAutoBilis(Elements els){
        if(els != null){
            for(Element element : els){
                if (element == els.last())
                    continue;

                String imgLink = element.select("img").first().attr("src");
                String link = element.select("a").first().attr("href");
                String title = element.select("h5").first().text();
                String price = element.getElementsByClass("price").first().text();

                Car temp = new Car(Integer.parseInt(price.substring(1,price.indexOf('€')).trim()), link, imgLink, title);
                cars.add(temp);
            }
        }
    }

    public void scrapeAutoGidas() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                Elements els;
                try {
                    Document doc = Jsoup.connect("https://autogidas.lt/skelbimai/automobiliai/?f_1%5B0%5D=Volkswagen&f_model_14%5B0%5D=Transporter&f_215=&f_216=&f_41=&f_42=1990&f_376=").get();
                    els = doc.getElementsByClass("list-item");
                    parseAutoGidas(els);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void parseAutoGidas(Elements els) {
        if (els != null) {
            for (Element element : els) {
                String imgLink = element.getElementsByAttribute("src").first().attr("src");
                String link = "https://autogidas.lt" + element.select("a").first().attr("href");
                String title = element.getElementsByClass("item-title").first().text();



                int price = Integer.parseInt(element
                        .getElementsByClass("item-price").first()
                        .select("meta").first()
                        .attr("content")
                );

                //filtering out already sold items
                if (imgLink.contains("parduota"))
                    continue;

                Car temp = new Car(price, link, imgLink, title);
                cars.add(temp);
            }
        }
    }

    public void addCar(Car car){
        cars.add(car);
    }

    public void statesFalse() {
        for (Car car : cars) {
            car.setActiveness(false);
        }
    }

    public void writeToCsv(Context context) {

        String text = "";
        String fileName = "testas.csv";

        for (Car i : cars) {
            text += i.toString();
        }


        try {
            OutputStreamWriter out =
                    new OutputStreamWriter(context.openFileOutput(fileName, 0));
            out.write(text);
            out.close();
            Toast.makeText(context, "File saved!", Toast.LENGTH_SHORT).show();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }


    //returns CarList of Cars that are not in a given list
    public CarList getUniqueCars(CarList list){
        CarList ans = new CarList();

        for(Car car1 : cars){
            if(!car1.isInGivenList(list)){
                ans.addCar(car1);
            }
        }
        return ans;
    }

    public void priceIncreasingOrder() {

        for (int i = 0; i < cars.size(); i++) {
            for (int j = i + 1; j < cars.size(); j++) {
                Car c = cars.get(j);
                if (cars.get(i).getPrice() > c.getPrice()) {
                    Collections.swap(cars, i, cars.indexOf(c));
                }
            }
        }
    }

    //TESTING
    public int getSize(){
        return cars.size();
    }
}


